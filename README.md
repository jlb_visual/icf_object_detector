**ICF Object Detector**

This repository contains the code developed by VISUAL50 for an object detection application based on homography comparison

In this stage, the software is composed of two applications: 

1. An object detection process that captures two video streams and compares an area of interest in each of them in order to detect objects that may protrude from a flat surface

2. A web server used to present and analyze the results

---

## Configure environments

Two virtual environments are required, one for each process:

```bash

$ virtualenv icf 
$ source icf/bin/activate
$ pip install -r requirements.txt
$ deactivate
$ virtualenv icf_web 
$ source icf_web/bin/activate
$ pip install -r requirements_web.txt
$ deactivate

```

Next config.py must be edited to define the folders where output and homograpy configuration files will be created.

La funcionalidad básica del proceso detector es la detección de objetos de volumen que sobresalgan sobre la superficie plana monitorizada. 

Para ello el proceso captura las imágenes de dos fuentes de vídeo que pueden ser cámaras rtsp o archivos de video. El programa necesita para funcionar los siguientes archivos de configuración:

Archivos de definición de área de interés (ROI) y matriz de homo grafía
Archivo de parámetros de procesamiento de imagen

Si estos archivos no existen para la opción en la que se está ejecutando el programa:
Si el programa está corriendo en modo interactivo, se solicita al usuario que se definan dichos archivos (en el modo de ejecución definitivo, no será posible el modo interactivo)
Si el programa no está corriendo el modo interactivo, se genera un mensaje de error y se termina la ejecución del programa

El programa puede ejecutarse en las siguientes modalidades: 
producción: las entradas se capturan de las direcciones IP de las cámaras de vídeo
Prueba de funcionamiento: este modo solo se utiliza para verificar que el proceso de detección se está ejecutando correctamente. El programa ejecuta de manera indefinida en un bucle infinito. el procesamiento de dos pequeños archivos de Video definidos en una carpeta de pruebas.
Procesamiento de archivos: el programa ejecuta el procesamiento de dos archivos de Vídeo facturados simultáneamente. Este modo es utilizado para verificar el funcionamiento con diferentes parámetros de procesado

La indicación del modo de funcionamiento se pasa al proceso mediante un parametro de ejecucion
Las direcciones de las cámaras se define en el archivo de configuracion

To do: el proceso detector principal indica el funcionamiento al servidor de monitorización, mediante la puesta a disposición del mismo de la última imagen procesada en un sistema de colas.

El servidor web de monitorización permite la siguiente funcionalidad:

Verificación de la salida de Video de las dos cámaras
Visualización de los resultados de una detección
Ejecución de una detección con parámetros de procesamiento específicos definidos mediante el interfaz de usuario
Verificación del funcionamiento del proceso de detección, mediante la lectura de la última imagen procesada
El servidor web se puede ejecutar. En modo pruebas, realiza la captura de los archivos de prueba de vídeo del sistema.


**Notas de configuración**

El identificador de la localización del sistema se denomina SITE_ID que se utiliza para identificar los archivos de áreas de recorte y la matriz de topografía. Estos archivos son fijos por cada localización y no se ha considerado en el programa la posibilidad de tener diferentes geometrías para la misma localización.

Los parámetros de procesamiento se almacenan en un archivo denominado params_00.txt. Este archivo se almacena como parte del código de la aplicación en la carpeta config.

El sistema permite la utilización de diferentes archivos de parámetros de procesamiento. Esto se define en la ejecución del detector mediante un parámetro de ejecución, así:

python main_detector.py -params params_00.txt

En esta versión ya no se permite la generación de parámetros de ejecución ni los archivos de geometría desde el detector en modo interactivo. Estos parámetros deben definirse desde la aplicación web.

**Despliegue del sistema**

***Sistema inicial***

La aplicación se instala sobre un sistema Ubuntu Versión xxx accesible vía SSH

Creación de usuarios

Se creará un usuario con nombre vdetector desde el que se realizará la instalación y se ejecutará los programas de detección y supervisión

Una vez creado el usuario, se deben descargar los archivos de códigos al directorio home del usuario desde bitbucket

Instalación de entornos virtuales para ejecución

To do

Despliegue de los procesos como servicios del sistema

Se copiarán los archivos e IXXX e I a la carpeta del sistema barra, etc./services

Se pueden lanzar los servicios con el comando

Xxx

Verificar la salida de los bloques del sistema con el comando
Xxxc
Si se ejecuta este comando en este momento, el sistema indicará que no está configurado:

Tu du: configurar errores en el lanzamiento, por falta de archivos de configuración

El siguiente paso para verificar el correcto despliegue es intentar ejecutar los procesos en modo de prueba para verificar que se realiza la detección y que el interfaz web está correctamente activado

Para ello en el archivo vas del usuario vdetector Se debe incluir el siguiente comando:

Export blablablá

A continuación, si el acceso al servidor es directo se puede abrir una pantalla en la consola del mismo y navegar a local House: 5000

Configuración de red y de las cámaras 

Nota importante:

En la situación actual el software se ha desarrollado como versión de prueba de concepto y la verificación del video se hace una vez por segundo, en un entorno real, esta veriifcación sería sin espera 
