**Instalacion de ICF Object Detector**


***Notas de instalación***

Todos los directorios tanto de código, como de escritura de archivos de salida, se encuentran en el mismo directorio dentro de la carpeta Home del usuario icf.

Se le debe verificar de todas maneras que la definición de directorios y archivos apunta a ellos correctamente

Una vez copiada la carpeta, se deben instalar los entornos virtuales de Python, tanto para el programa de detección, como para el servidor web de supervisión
El archivo de configuración. config.py también incluye las direcciones IP de las cámaras de vídeo. Se puede verificar la correcta conexión de las mismas desde el servidor web en las opciones. Live Video

Se puede verificar la correcta instalación de los entornos virtuales, lanzando los ejecutables de Pyton, del servidor web y del detector y verificando que llegan a ejecutarse, aunque den errores.

Se puede ejecutar el programa de detección en modo de pruebas, para verificar que se ejecuta correctamente y, paralelo, ejecutar el servidor web para verificar desde el exterior, la conectividad y el funcionamiento combinada de ambos programas.

A continuación, debe configurarse las áreas de detección de ambas cámaras, mediante la opción correspondiente del servidor web.

***Notas de la instalación de pruebas.***

La instalación actual está orientada fundamentalmente a la verificación de los resultados. A tal fin ha facilitado la escritura en archivos de salida el resultado del proceso de detección. En una instalación en producción se deben optimizar algunos Factores del diseño de la instalación.

Posibles temas pendientes para optimización:

El detector realiza el proceso de detección una vez por segundo, con objeto de poder tener archivos de salida fácilmente manejables. En una instalación real se debe buscar el punto de equilibrio óptimo entre capacidad de procesamiento y rapidez en la detección de emergencias.

El proceso de detección escribe cada 10 segundos el resultado del mismo en una imagen en la carpeta de monitorización. El objetivo de esta función es poder verificar el funcionamiento del proceso de detección desde el servidor web.

***Procedimiento***

El punto de partida es un servidor Ubuntu con version 22 o posterior.

La configuración de red debe permitir el acceso ssh y tner abiero el puerto 5000 para conexiones http.

La aplicación está configurada para un usuario de nombre icf en el que se copian los archivos de código y los directorios de salida en un directorio en su directorio raiz (home)

Si no se siguen estas indicaciones se deben revisar los archivos de configuración (config.py) y los de lanzamiento de procesos.

Se necesita instalar python, pip y virtualenv.

A continuación se deben copiar los archivos.

```bash
sudo mount /dev/sdb /mnt/usb_drive
ls /mnt/usb_drive
cp -r /mnt/usb_drive/icf_object_detector .

A continuación nos posicionamos en el directorio icf_object_detector/src que almacena todo el código
del sistema


cd icf_object_detector
cd src

Creamos dos entonos virtuales de desarrollo: uno para el detector y otro para la web, se instalan los paquetes necesarios para la ejecucion de cada p

```bash
virtualenv icf_detector
source icf_detector/bin/activate
pip install -r requirements.txt

virtualenv icf_web
source icf_web/bin/activate
pip install -r requirements_web.txt

```
En este momento se puede verificar que los dos ejecutables pueden correr bien en cada entorno

Para la correcta ejecución de los programas se debe verificar que las variables de ejecucíón del archivo config.py están correctamente configuradas. En particular las direcciones IP de las cámaras y los directorios donde se almacenan los archivos de configuración y de video de salida

```bash
SITE_ID = "prod0"

camera0 = "rtsp://admin:1234abcd@192.168.1.64"
camera1 = "rtsp://admin:clara222@192.168.1.65"
	
OUTPUT_VIDEO_FOLDER_REL = "../prod/media"
OUTPUT_VIDEO_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), OUTPUT_VIDEO_FOLDER_REL))

RESULTS_FOLDER_REL = "../prod/results"
RESULTS_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), RESULTS_FOLDER_REL))

MONITOR_FOLDER_REL = "../prod/monitor"
MONITOR_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), MONITOR_FOLDER_REL))

```

En este momento se deberían poder ejecutar los ejecutables de detección y del servidor web:

```bash
source icf_detector/bin/activate
python main_detector.py


source icf_web/bin/activate
python runserver.py

```
A continuación se deben configurar los archivs de servcio del sistema que permiten que los programas se ejecuten de manera autónomo y se reinicien cada vez que se arranque el sistema.

Para ello se copian los archivos de definicion de servcios icf_detector.service e icf_web.service al directorio /etc/systemd/system

```bash
sudo cp  icf_detector.service /etc/systemd/system
sudo cp  icf_web.service /etc/systemd/system

```

A continuación se lanzan los dos ejecutables como servcios del sistema

```bash
sudo systemctl daemon-reload
sudo service icf_detector start
sudo service icf_web start
```

En este momento se deberían estar ejecutando los dos programas.

Para verificar la correcta ejecución se puede abrir en un navegador el puerto 5000 de la direccion IP del servidor y se podrá visualizar la aplicaición de supervisión icf_web 



