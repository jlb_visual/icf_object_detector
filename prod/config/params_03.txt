{
    "bin_thr": 32,
    "filename": "params_03.txt",
    "gauss_sigma": 1,
    "gauss_w_h_display_value": 1,
    "it_close0": 1,
    "it_close1": 10,
    "it_open": 2,
    "kernel_display_value": 5,
    "max_area": 500000,
    "min_area": 5000
}