# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 20:29:00 2020

@author: Miguel

Version de JLB y AP

Programa detector de objetos para aplicación ICF

Bucle detector principal utilizado en el proyecto icf_object_detector

Este programa puede ejecutarse de forma interactiva o en modo batch.

El programa utiliza los siguientes parámetros:

SITE_ID: utilizado para nombrar los archivos de geometría (matriz de puntos y homografía)
Si no hay archivos de geometría disponibles y el programa se ejecuta en modo interactivo,
Se le pedirá al usuario que cree nuevos archivos.


params_id: un identificador utilizado para seleccionar diferentes opciones de parámetros para probar
detecciones alternativas. Por defecto, esta es una cadena igual a "param_00"

El usuario puede indicar el uso de un archivo de parámetros diferente con el argumento -p

Se utilizan dos carpetas de entrada/salida:
     param_folder almacena información de procesamiento de datos como puntos roi y matriz de homogeneidad
     ouput_folder almacena archivos de vídeo y archivos de texto de detección

El proceso principal realiza las siguientes acciones.

1. recibe dos flujos de entrada de dos cámaras IP o de archivos

2. recorta y transforma la imagen de la segunda secuencia de vídeo según una matriz de homografía
definido en los parámetros del programa. Si no se encuentran los archivos de homografía y se ejecuta el programa
De forma iterativa, se solicita al usuario que los cree.

3. compara la primera imagen recortada con la transformación de homografía de la segunda
y si hay discrepancias, estas se interpretan como un objeto en la superficie plana.

4. Si la variable record_output se establece en verdadero, se almacenan tres archivos de vídeo: los dos primeros corresponden
a los flujos de entrada y el tercero es una combinación de las dos imágenes de entrada juntas
con una superposición de los dos para indicar los resultados de la detección. También un archivo de texto de salida.
se registra con los resultados de la detección

El programa puede ejecutarse con los siguientes parametros:
- -i interactivo: presenta los resultados en una ventana 
- 

Para fines de prueba, el sistema utiliza un identificador de referencia para almacenar diferentes resultados que podrían ser
obtenidos de los mismos archivos, por lo tanto, si ref se establece en test01, los puntos de salida y los archivos de homologación se denominan:
test01_pts0.txt, test01_pts1.txt y test01_matrix.txt. Los parámetros de detección también se almacenan.
en un archivo json llamado proc_params

Se utiliza la misma notación para el vídeo y el archivo de resultados.

"""

import sys
import cv2
import numpy as np
import argparse
import os.path
import pathlib
import math
import imutils
from datetime import datetime
import json

from image_process.VideoCaptureBufferless import VideoCapture 

from image_process.detector import ProcParams, detect_object, build_output_image, build_composed_results

from image_process.homography import getRoiframe, calculate_matrix_from_rois

from image_process.proc_gui import selectROI

from config import RESULTS_FOLDER, MONITOR_FOLDER, OUTPUT_VIDEO_FOLDER, SITE_ID

from config import camera0, camera1

# parametros de entrada

ap = argparse.ArgumentParser()

# input parameters

# Los parametros admitidos son los siguientes:

# Run interactive or in batch mode
ap.add_argument("-int", "--interactive", action='store_true', default= False, required=False, help="Run iteractively")

# record output
ap.add_argument("-ro", "--record_output", action="store_true", default= False, help='Record ouput')

# record output
ap.add_argument("-test", "--test", action="store_true", default= False, help='Basic test with dummy files')

# record output
ap.add_argument("-proc_files", "--proc_files", action="store_true", default= False, help='Process video files')

# Test reference identifier
ap.add_argument("-params", "--params_file", type=str, required=False, help="Processing parameters identifier")

ap.add_argument("-file0", "--file0", type=pathlib.Path, metavar="file_0_path", required=False, help="Video file0 for file_proc option")
ap.add_argument("-file1", "--file1", type=pathlib.Path, metavar="file_1_path", required=False, help="Video file1 for file_proc option")

# parse args and initialize variables
args = vars(ap.parse_args())

interactive = args['interactive']
record_output = args['record_output']

print("interactive:", interactive)
print("record_output:", record_output)

if args['test'] and args['proc_files']:
    print("[ERROR] Conflicting arguments test and proc_files")

# production is the default value
EXEC_MODE = "PROD"

if args['test']:
    EXEC_MODE = "TEST"

if args['proc_files']:
    EXEC_MODE = "PROC_FILES"


if args['params_file']:
    params_file = args['params_file']
else:
    params_file = "params_00.txt"

# Verificar el valor de EXEC_MODE y definir las variables correspondientes
if EXEC_MODE == 'PROD':

    CONFIG_FOLDER_REL = "../prod/config"
    CONFIG_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), CONFIG_FOLDER_REL))
    
elif EXEC_MODE == 'PROC_FILES':

    MONITOR_FOLDER_REL = "../test_files/monitor"
    MONITOR_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), CONFIG_FOLDER_REL))

    CONFIG_FOLDER_REL = "../prod/config"
    CONFIG_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), CONFIG_FOLDER_REL))
    
    file0 =  args['file0']
    file1 =  args['file1']

    if file0 is None or file1 is None:
        print("[ERROR] Input files are needed in proc_files mode")

elif EXEC_MODE == 'TEST':
    # test environment
    SITE_ID = "testSite"

    CONFIG_FOLDER_REL = "../test_files/config"
    CONFIG_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), CONFIG_FOLDER_REL))
    
    OUTPUT_VIDEO_FOLDER_REL = "../test_files/media"
    OUTPUT_VIDEO_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), OUTPUT_VIDEO_FOLDER_REL))

    RESULTS_FOLDER_REL = "../test_files/results"
    RESULTS_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), RESULTS_FOLDER_REL))

    # Las pruebsa se hacensobre los dos archivos que se incuyen en la distribucion
    file0 = os.path.join(OUTPUT_VIDEO_FOLDER, "prueba20220124_054308_output_video0.mp4" )
    file1 = os.path.join(OUTPUT_VIDEO_FOLDER, "prueba20220124_054308_output_video1.mp4" )

proc_params = ProcParams()

def create_output_files(id_str, img0, img1, img_0_1):
    """
    Create video and text ouput files
    
    This function is called when the program starts and every time the output files are reset
    
    Returns open files for video recording and a text file for storing results

    """
    print("[INFO] Creating output files")
    date_str = datetime.now().strftime('%Y%m%d_%H%M%S_')

    output_video_file0 = os.path.join(OUTPUT_VIDEO_FOLDER, SITE_ID + "_" +  date_str + "output_video0.mp4")
    output_video_file1 = os.path.join(OUTPUT_VIDEO_FOLDER, SITE_ID + "_" +  date_str + "output_video1.mp4")
    results_video_file = os.path.join(OUTPUT_VIDEO_FOLDER, SITE_ID + "_" + id_str + "_" +  date_str + "results.mp4")
    output_results_file = os.path.join(RESULTS_FOLDER, SITE_ID + "_" + id_str + "_"+  date_str +"results.txt")
    
    output_fps = 24
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    
    #
    # assume image0 and image1 are same size, if not
    # this should be changed 
    #

    image_size = (img0.shape[1], img0.shape[0])
    
    out0 = cv2.VideoWriter(output_video_file0, fourcc, output_fps, image_size)
    out1 = cv2.VideoWriter(output_video_file1, fourcc, output_fps, image_size)

    out_size = (img_0_1.shape[1], img_0_1.shape[0])

    out_results = cv2.VideoWriter(results_video_file, fourcc, output_fps, out_size)
    
    results_file = open(output_results_file,"w")
    #results_file.write(param_info_str)
    
    return out0, out1, out_results, results_file


def release_output_files(out0, out1, out_results, results_file):
    """ Close text and video output files

    This is performed when results files are rotated
    """
    out0.release()
    out1.release()
    out_results.release()
    out_results = None
    results_file.close()


def verify_output_folders():
    """
    Verify existence of output folders and create them if not present
    """
    print("[INFO] Verifying folders")
    if not os.path.exists(CONFIG_FOLDER):
        try:
            os.mkdir(CONFIG_FOLDER)
            print("[INFO] Creating CONFIG_FOLDER")
        except:
            print("[ERROR] Failed creating config folder: ", CONFIG_FOLDER)
            exit()

    if not os.path.exists(OUTPUT_VIDEO_FOLDER):
        try:
            os.mkdir(OUTPUT_VIDEO_FOLDER)
            print("[INFO] Creating OUTPUT_VIDEO_FOLDER")
        except:
            print("[ERROR] Failed creating results folder: ", OUTPUT_FOLDER)
            exit()

def printVideoStats(cap_):
    #print video statistics ; before release of cap ; else no value
    print ("Frame Count   : " + str(cap_.get(7)))  #CV_CAP_PROP_FRAME_COUNT 
    print ("Format        : " + str(cap_.get(8)))  #CV_CAP_PROP_FORMAT)
    print ("Height        : " + str(cap_.get(4)))  #CV_CAP_PROP_FRAME_HEIGHT)
    print ("Width         : " + str(cap_.get(3)))  #CV_CAP_PROP_FRAME_WIDTH)
    print ("Mode          : " + str(cap_.get(9)))  #CV_CAP_PROP_MODE)
    print ("Brightness    : " + str(cap_.get(10))) #CV_CAP_PROP_BRIGHTNESS)
    print ("Fourcc        : " + str(cap_.get(6)))  #CV_CAP_PROP_FOURCC)
    print ("Contrast      : " + str(cap_.get(11))) #CV_CAP_PROP_CONTRAST)
    print ("FramePerSec   : " + str(cap_.get(5)))  #CV_CAP_PROP_FPS)
    
    #Get duration of the video
    FrameCount = cap_.get(7)    #CV_CAP_PROP_FRAME_COUNT 
    FrameperSecond =cap_.get(5) #CV_CAP_PROP_FPS
    DurationSecond = FrameCount/FrameperSecond
    if FrameperSecond>0:
        print ("FrameDuration : " + str(DurationSecond) + "seconds")

def calculate_matrix(img0_ref, img1_ref):
    """
    Rquest ROI from user and calculate honography matrix

    ROIS are obtained by defining the polygons in a display window

    This activity is now performed via web. Left for archival purpose
    """
    
    # Select ROI 1 in gui
    roi_pts0 =  selectROI(img0_ref)
    print("roi_pts0:", roi_pts0)
    
    img0_roi_points = np.array(roi_pts0, np.int32).reshape((-1, 1, 2))
    img0_roi = getRoiframe(img0_ref, img0_roi_points)
    
    # display ROI 1
    cv2.imshow('img0_ref', imutils.resize(img0_ref, width=800))
    cv2.imshow('img0_roi', imutils.resize(img0_roi, width=800))
    
    # Select ROI 2 in gui
    roi_pts1 = selectROI(img1_ref)
    img1_roi_points = np.array(roi_pts1, np.int32).reshape((-1, 1, 2))
    img1_roi = getRoiframe(img1_ref, img1_roi_points)

    # Display ROI 2
    cv2.imshow('img1_ref', imutils.resize(img1_ref, width=800))
    cv2.imshow('img1_roi', imutils.resize(img1_roi, width=800))

    # Calculate homography matrix
    transf, img3, msg = calculate_matrix_from_rois(img0_roi, roi_pts0, img1_roi, roi_pts1, display = True)

    return roi_pts0, roi_pts1, transf


def create_trackbars(
    gauss_w_h_initial,
    gauss_sigma_initial,
    bin_thr_initial,
    kernel_initial,
    it_close0_initial,
    it_open_initial,
    it_close1_initial,
    min_area_initial,
    max_area_initial):
    # Create a black image, a window
    cv2.namedWindow('Trackbars', cv2.WINDOW_NORMAL)
    cv2.createTrackbar('gauss_w_h','Trackbars',gauss_w_h_initial,100,on_trackbar_change)
    cv2.createTrackbar('gauss_sigma','Trackbars',gauss_sigma_initial,500,on_trackbar_change)
    cv2.createTrackbar('bin_thr','Trackbars',bin_thr_initial,255,on_trackbar_change)
    cv2.createTrackbar('kernel','Trackbars',kernel_initial,255,on_trackbar_change)
    cv2.createTrackbar('it_close0','Trackbars',it_close0_initial,20,on_trackbar_change)
    cv2.createTrackbar('it_open','Trackbars',it_open_initial,20,on_trackbar_change)
    cv2.createTrackbar('it_close1','Trackbars',it_close1_initial,20,on_trackbar_change)
    cv2.createTrackbar('min_area','Trackbars',min_area_initial,10000,on_trackbar_change)
    cv2.createTrackbar('max_area','Trackbars',max_area_initial,500000,on_trackbar_change)


def on_trackbar_change(value):
    global proc_params

    print("Setting proc. params")
    proc_params = ProcParams(cv2.getTrackbarPos('gauss_w_h','Trackbars'),
        cv2.getTrackbarPos('gauss_sigma','Trackbars'),
        cv2.getTrackbarPos('bin_thr','Trackbars'),
        cv2.getTrackbarPos('kernel','Trackbars'),
        cv2.getTrackbarPos('it_close0','Trackbars'),
        cv2.getTrackbarPos('it_open','Trackbars'),
        cv2.getTrackbarPos('it_close1','Trackbars'),
        cv2.getTrackbarPos('min_area','Trackbars'),
        cv2.getTrackbarPos('max_area','Trackbars')
        )
    print("Proc. params:", proc_params)

def main_loop():
    """
    Initialize program variables and parameters
    """
    
    """
    print(proc_params.toJSON())
    #proc_params.write_to_json_file("test.txt")

    #print("Wrote proc_params")

    proc_params.read_from_json_file("test.txt")

    exit()
    """


    print("interactive:", interactive)

    if record_output:
        #
        # check if output folders exist and create them if not
        #
        verify_output_folders() 


    # por ahora no vamos a consderr esta opcion
    show_track_bars = False
    
    now = datetime.now()
    
    cap0 = None
    cap1 = None
    
    fps0 = 0
    fps1 = 0
    
    # Set up input streams
    
    if EXEC_MODE == "PROD":
        print("[INFO] Connecting url " + str(camera0), datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
 
        #
        # in case of live video, use bufferless VideoCapture class
        #

        cap0 = VideoCapture(camera0)
        if cap0.isOpened() == False:
            print("[ERROR] ", camera0 + " -> CONNECTION FAIL", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
            sys.exit()

        print("[INFO] camera 0 OK", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )

        print("[INFO] Connecting url " + str(camera1) + " ...")
        cap1 = VideoCapture(camera1)
        if cap1.isOpened() == False:
            print("[ERROR] ",camera1 + " -> CONNECTION FAIL", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
            sys.exit()
        print("[INFO] camera 1 OK")

        fps1 = cap1.getFPS()


    else:               
        #
        # in case of video files, use standard cv2.VideoCapture class
        #

        print("[INFO] Checkin video files", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
        if not os.path.exists(file0):
            print("[ERROR]" + str(file0) + " -> PATH does not exist")
            sys.exit()
        
        cap0 = cv2.VideoCapture(file0)
        #cap0.set(cv2.CAP_PROP_POS_FRAMES, video_offset0)
        fps0 = round(float(cap0.get(cv2.CAP_PROP_FPS)), 2)

        #printVideoStats(cap0)
        
        if(fps0<1): 
            print("[ERROR] FPS0 ERROR (fps<1): " + str(fps0))
            sys.exit()
        
        if not os.path.exists(file1):
            print(str(file1) + " -> PATH does not exist")
            sys.exit()
            
        cap1 = cv2.VideoCapture(file1)
        #cap1.set(cv2.CAP_PROP_POS_FRAMES, video_offset1)
        fps1 = round(float(cap1.get(cv2.CAP_PROP_FPS)), 2)

        #printVideoStats(cap1)
        
        if(fps1<1): 
            print("[ERROR] FPS1 ERROR (fps<1): " + str(fps1))
            sys.exit()

    #
    # read the first two images in case they are needed for homography
    #

    ret0, img0 = cap0.read()
    ret1, img1 = cap1.read()
    
    fps0 = 0
    fps1 = 0

    #
    # load or generate homography info
    #
    
    h_file_path = os.path.join(CONFIG_FOLDER, SITE_ID + "_matrix_file.txt")
    roi0_file_path = os.path.join(CONFIG_FOLDER,SITE_ID + "_pts0.txt")
    roi1_file_path =  os.path.join(CONFIG_FOLDER,SITE_ID + "_pts1.txt")


    if os.path.exists(roi0_file_path) and \
        os.path.exists(roi1_file_path) and \
        os.path.exists(h_file_path):
        
        # 
        # if roi and homography files exist, read them
        # 
        print("[INFO] Reading stored roi and homography files", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
        try:
            roi_pts0 = np.loadtxt(roi0_file_path)
            roi_pts0 = np.array(roi_pts0, np.int32).reshape((-1, 1, 2))
            roi_pts1 = np.loadtxt(roi1_file_path)
            roi_pts1 = np.array(roi_pts1, np.int32).reshape((-1, 1, 2))
            h_matrix = np.loadtxt(h_file_path)
        except:
            print("[ERROR] Error reading geometry files", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )

    else:
        if not interactive:
            #
            # if running in batch points and homography matrix must be present
            #
            print("[ERROR] No points or homograpphy file found")
            print("[ERROR] Some file missing:\n",roi0_file_path,"\n",roi1_file_path,"\n",h_file_path)
            exit()


        print("[INFO] Image crop areas must be defined")

        roi_pts0, roi_pts1, h_matrix = calculate_matrix(img0, img1)

        #
        # save roi and matrix files, this is always done
        # 
        np.savetxt(roi0_file_path, roi_pts0)
        np.savetxt(roi1_file_path, roi_pts1)
        np.savetxt(h_file_path,h_matrix) 

        roi_pts0 = np.array(roi_pts0, np.int32).reshape((-1, 1, 2))  
        roi_pts1 = np.array(roi_pts1, np.int32).reshape((-1, 1, 2))


    #
    # load or generate parameter info
    #

    params_file_path = os.path.join(CONFIG_FOLDER, params_file)
    
    if os.path.exists(params_file_path):
        print("[INFO] Reading params", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
        proc_params.read_from_json_file(params_file_path)
    
    else:
        if params_file == "params_00.txt":
            print("[WARNING] Proc params params_00 not found, using default values ")
            proc_params.initialize_default()
            proc_params.filename = "params_00.txt"
            print(proc_params.toJSON())
            print("[INFO] Saving params_00", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
            proc_params.write_to_json_file(params_file_path)
        else:
            print("[ERROR] Proc params file %s not found. Exiting "%params_file_path, datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
            exit()


        """
        if not interactive:
            #
            # if running in batch points and homography matrix must be present
            #
            print("[ERROR] No parameter file found")
            print("[ERROR] File missing:\n",params_file_path)
            exit()

        if interactive:
            print("[INFO] You can set values via trackbars and save them by pressing S or s")
            show_track_bars = True
        """
    # write args summary to output text file and print it on screen

    #print(param_info_str)

    print(proc_params.toJSON())

    #proc_params.write_to_json_file(params_file_path)

    # do a first detection in order to build img_0_1 needed for creating 
    # video files


    something_detected, imgCmp, blended, img_bins, closing0, bin_out =\
            detect_object(img0, img1,
                roi_pts0, 
                roi_pts1, 
                h_matrix,
                proc_params)

    print("building test image")
    img_0_1 =build_output_image(img0, img1, imgCmp, blended, img_bins, closing0, bin_out, show_option="D1") 

    if interactive:
        cv2.imshow("results",img_0_1)
        #print("here")
        key = cv2.waitKey(0)
        if key == 27:                    
            cv2.destroyAllWindows()
            exit()

    if record_output:
        # 
        # generate output files
        # 
        params_id = params_file.replace(".txt","")
        print("[INFO] Generating output files")
        out0, out1, out_results, results_file = \
            create_output_files(params_id, img0, img1, img_0_1)
    """
    if(show_track_bars):
        create_trackbars(
                proc_params.gauss_w_h_display_value,
                proc_params.gauss_sigma,
                proc_params.bin_thr,
                proc_params.kernel_display_value,
                proc_params.it_close0,
                proc_params.it_open,
                proc_params.it_close1,
                proc_params.min_area,
                proc_params.max_area)
    """

    # Monitoring is used to write a temporary image in a memory file so that it can be read from the 
    # web server
    monitoring = True 

    #
    # cmd_play is used to stop the image from the gui
    #
    cmd_play = True

    show_option = "D1"

    last_detection = False

    frame_written = 0

    now = datetime.now()
    last_second = now.second
    last_minute = now.minute
    last_hour = now.hour 

    exit_loop = False

    while not exit_loop:
        #
        # main detection loop
        #

        if(cmd_play):
            #
            # read next frame
            # 
            if EXEC_MODE != "PROD":
                #
                # in case of video files, read next frame
                #
                try:
                    ret0, img0 = cap0.read()
                    ret1, img1 = cap1.read()
                except:
                    if EXEC_MODE == "test":
                        # infinite loop reset test files and keep going
                        # TO DO
                        break
                    else:
                        exit_lopp = True 
                        break
            else:
                #
                # for live video read frames until change of seconds
                # this can be changed if more or less time interval is needed
                #      
                while True:
                    ret0, img0 = cap0.read()
                    ret1, img1 = cap1.read()        
                    now = datetime.now()
                    if now.second != last_second:
                        break

        # if not cmd play do nothing and wait for the user to resume

        # Generate a video file every hour

        if now.hour != last_hour and record_output:
                    #
                    # close files and create new ones
                    #
                    release_output_files(out0, out1, out_results, results_file)
                    frame_written = 0
                    params_id = params_file.replace(".txt","")
                    out0, out1, out_results, results_file = \
                        create_output_files(params_id, img0, img1, img_0_1)
            
        last_second = now.second
        last_hour = now.hour

        frameId0 = cap0.get(1) #current frame number
        frameId1 = cap1.get(1) #current frame number

        frame_written += 1
    
        something_detected, imgCmp, blended, img_bins, closing0, bin_out =\
            detect_object(img0, img1,
                roi_pts0, 
                roi_pts1, 
                h_matrix,
                proc_params)

        if (last_detection != something_detected):
            if something_detected:
                print("[INFO] Object detected frame:%d"%frame_written)
            else:
                print("[INFO] End of detection frame:%d"%frame_written)

        if record_output or interactive or monitoring:
            #
            # build image to record
            #
            img_0_1 =build_output_image(img0, img1, imgCmp, blended, img_bins, closing0, bin_out, show_option)
            
            # img2 = build_composed_results(blended, img_bins, closing0, bin_out)
        
            # Write to results video
            if record_output:

                out0.write(img0)
                out1.write(img1)
                out_results.write(img_0_1)

                #
                # write to output txt
                #

                if (last_detection != something_detected):
                    if something_detected:
                        results_file.write("[INFO] Object detected frame:%d\n"%frame_written)
                    else:
                        results_file.write("[INFO] End of detection frame:%d\n"%frame_written) 

            if monitoring:
                if now.second % 10 == 0:
                    print("[INFO] writing to monitor", datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] )
                    image_path = os.path.join(MONITOR_FOLDER, "monitor.png")
                    cv2.imwrite(image_path, img_0_1)

            if interactive:  
                cv2.imshow("results",img_0_1)
                #print("here")
                key = cv2.waitKey(1)
                if key == 27:                    
                    cv2.destroyAllWindows()
                    break
                """
                key = cv2.waitKey(1)
                if key == 27:                    
                    cv2.destroyAllWindows()
                    break

                if key == ord('s'):
                    #save proc params
                    break

                if key == ord('w'): #wait / not wait
                    cmd_play = not cmd_play
                """
        last_detection = something_detected

    cap0.release()
    cap1.release()

    if record_output:
        print("[INFO] Releasing files")
        out0.release()
        out1.release()
        out_results.release()
        results_file.close()

    if interactive:  
        cv2.destroyAllWindows()

    sys.exit()

if __name__ == "__main__":
    main_loop()