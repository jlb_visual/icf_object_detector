# -*- coding: utf-8 -*-
"""
Created on Sat May 30 12:06:14 2020

@author: Miguel
"""
import cv2
import numpy as np
import os
from imutils import paths

def resizeImage(src, scale_percent):
    
    #calculate the percent of original dimensions
    width = int(src.shape[1] * scale_percent / 100)
    height = int(src.shape[0] * scale_percent / 100)
    
    # dsize
    dsize = (width, height)
    
    # resize image
    output = cv2.resize(src, dsize)
    
    #cv2.imshow('resized',output) 
    return output

def ResizeWithAspectRatio(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized

def putTitle(img, text_x, text_y, text, font_scale = 1, font = cv2.FONT_HERSHEY_SIMPLEX, font_color=(0, 0, 0), text_thickness=1, background_color = (255, 255, 255), background_padding = 5):

    # get the width and height of the text box
    (text_width, text_height) = cv2.getTextSize(text, font, fontScale=font_scale, thickness=text_thickness)[0]
    
    # set the text start position
    text_offset_x = text_x
    text_offset_y = text_y
    
    # make the coords of the box with a small padding of two pixels
    box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + background_padding*2, text_offset_y - text_height - background_padding*2))
    
    cv2.rectangle(img, box_coords[0], box_coords[1], background_color, cv2.FILLED)
    cv2.putText(img, text, (text_offset_x+background_padding, text_offset_y-background_padding), font, fontScale=font_scale, color=font_color, thickness=text_thickness)

def putFrameNumberLabel(frame_, frame_n_, font_scale = 1, text_thickness = 2, background_padding = 10):
    frame_out_h, frame_out_w, frame_out_ch = frame_.shape
    frames_text = '%s%.f'%('Frame:',frame_n_)
    frames_text_font = cv2.FONT_HERSHEY_SIMPLEX
    frames_text_font_scale = font_scale
    frames_text_text_thickness = text_thickness
    frames_text_background_padding = background_padding
    
    (frames_text_width, frames_text_height) = cv2.getTextSize(frames_text, frames_text_font, fontScale=frames_text_font_scale, thickness=frames_text_text_thickness)[0]
    putTitle(frame_, frame_out_w - frames_text_width - frames_text_background_padding*2, 54, frames_text, font_scale = frames_text_font_scale, font = frames_text_font, font_color=(255, 255, 255), text_thickness=frames_text_text_thickness, background_color = (0, 0, 0), background_padding = 10)
    
def putLabel(img, padding_x, padding_y, text, font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = 1, font_color=(255, 255, 255), thickness = 2, background_color = (0, 0, 0), background_padding = 10):
    frame_out_h, frame_out_w, frame_out_ch = img.shape
    frames_text = text
    frames_text_font = font
    frames_text_font_scale = font_scale
    frames_text_text_thickness = thickness
    frames_text_background_padding = background_padding
    
    (frames_text_width, frames_text_height) = cv2.getTextSize(frames_text, frames_text_font, fontScale=frames_text_font_scale, thickness=frames_text_text_thickness)[0]
    putTitle(img, frame_out_w - frames_text_width - frames_text_background_padding*2 - padding_x, frames_text_height+frames_text_background_padding*2+padding_y, frames_text, font_scale = frames_text_font_scale, font = frames_text_font, font_color=font_color, text_thickness=frames_text_text_thickness, background_color = background_color, background_padding = frames_text_background_padding)
    
    
def drawGrid(img, grid_w_div = 4, grid_h_div = 4, grid_color = (255, 255, 255), grid_thickness = 3):
    
    image = img.copy()
    h = image.shape[0]
    w = image.shape[1]
    
    for i in range(grid_w_div+1):
        cv2.line(image, (int(i*w/grid_w_div), 0), (int(i*w/grid_w_div), h), grid_color, thickness=grid_thickness)
    for i in range(grid_h_div+1):
        cv2.line(image, (0, int(i*h/grid_h_div)), (w, int(i*h/grid_h_div)), grid_color, thickness=grid_thickness)
        
    return image

def drawCircle(img, point, center_circle_radius, center_circle_color = (255, 0, 0), center_circle_thickness = 3):
    
    image = img.copy()
    cv2.circle(image, point, center_circle_radius, center_circle_color, center_circle_thickness) 
    return image

def drawCross(img, point, cross_len = 10, cross_color = (255, 0, 0), cross_thickness = 5):
    
    image = img.copy()
    xc = point[0]
    yc = point[1]
    
    cv2.line(image, (xc, yc), (xc, yc+cross_len), cross_color, thickness=cross_thickness)
    cv2.line(image, (xc, yc), (xc+cross_len, yc), cross_color, thickness=cross_thickness)
    cv2.line(image, (xc, yc), (xc, yc-cross_len), cross_color, thickness=cross_thickness)
    cv2.line(image, (xc, yc), (xc-cross_len, yc), cross_color, thickness=cross_thickness)
        
    return image
  
def imgBlend(src, dst, roi_pts, roi_maskBin, alpha=0.5):

    beta = 1 - alpha
    blended = cv2.addWeighted(src, alpha, dst, beta, 1.0)
    blended_mask = cv2.bitwise_and(blended, roi_maskBin)

    pts = np.array(roi_pts, np.int32) 
    pts = pts.reshape((-1, 1, 2)) 
    blended_mask_rect = cv2.boundingRect(pts)
    blended_mask_crop = blended_mask[blended_mask_rect[1]: blended_mask_rect[1] + blended_mask_rect[3], blended_mask_rect[0]: blended_mask_rect[0] + blended_mask_rect[2]]

    return blended, blended_mask, blended_mask_crop

def printVideoStats(cap):
    print("")
    print("***************")
    print("* VIDEO STATS *")
    print("***************")
    #print video statistics ; before release of cap ; else no value
    print ("Frame Count   : " + str(cap.get(7)))  #CV_CAP_PROP_FRAME_COUNT 
    print ("Format        : " + str(cap.get(8)))  #CV_CAP_PROP_FORMAT)
    print ("Height        : " + str(cap.get(4)))  #CV_CAP_PROP_FRAME_HEIGHT)
    print ("Width         : " + str(cap.get(3)))  #CV_CAP_PROP_FRAME_WIDTH)
    print ("Mode          : " + str(cap.get(9)))  #CV_CAP_PROP_MODE)
    print ("Brightness    : " + str(cap.get(10))) #CV_CAP_PROP_BRIGHTNESS)
    print ("Fourcc        : " + str(cap.get(6)))  #CV_CAP_PROP_FOURCC)
    print ("Contrast      : " + str(cap.get(11))) #CV_CAP_PROP_CONTRAST)
    print ("FramePerSec   : " + str(cap.get(5)))  #CV_CAP_PROP_FPS)
    
    #Get duration of the video
    FrameCount = cap.get(7)    #CV_CAP_PROP_FRAME_COUNT 
    FrameperSecond =cap.get(5) #CV_CAP_PROP_FPS
    DurationSecond = FrameCount/FrameperSecond
    if FrameperSecond>0:
        print ("FrameDuration : " + str(DurationSecond) + " seconds")
        
def create_blankImg(width, height, rgb_color=(0, 0, 0)):
    """Create new image(numpy array) filled with certain color in RGB"""
    # Create black blank image
    image = np.zeros((height, width, 3), np.uint8)

    # Since OpenCV uses BGR, convert the color first
    # color = tuple(reversed(rgb_color))
    color = tuple((rgb_color))
    # Fill image with color
    image[:] = color

    return image

def load_images_from_folder(folder, resolution='resP', resolution_val=100, max_img=0, gray_mode=False):
    images = []
    i = 0
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            if(max_img>0)and(max_img<=i):
                break
            print("[" + str(i) + "]image read: " + str(filename))
            if(resolution=='resP')  : img = resizeImage(img, resolution_val)
            elif(resolution=='resW'): img = ResizeWithAspectRatio(img, width = resolution_val)
            elif(resolution=='resH'): img = ResizeWithAspectRatio(img, height = resolution_val)
            
            if(gray_mode):
                img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            images.append(img)
            i = i + 1
    return images

def stringBar(percentage, barLen = 10):
    bar=""
    for j in range(barLen):
        # bar = str(percentage) + " " + str(int((percentage/10)%((j+1))))
        if(j<int((percentage/barLen))):
            bar=bar+"="
        else:
            bar=bar+" "
                
    return "[" + bar + "]"

