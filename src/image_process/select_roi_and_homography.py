# -*- coding: utf-8 -*-
"""
Created on Mon May 18 17:03:11 2020

@author: Miguel

#prueba_de_concepto_40 - homography estimation enhanced

"""

import sys
import cv2
import numpy as np
import time
import joblib
import pyclipper
import imutils 
from imutils.video import FileVideoStream
from imutils.video import FPS
from threading import Thread
import random
#import padtransf
import concurrent.futures
import threading
import logging
import threading
import queue
import math
import os.path
import keyboard

def nothing(x):
    pass

def resizeImage(src, scale_percent):
    
    #calculate the percent of original dimensions
    width = int(src.shape[1] * scale_percent / 100)
    height = int(src.shape[0] * scale_percent / 100)
    
    # dsize
    dsize = (width, height)
    
    # resize image
    output = cv2.resize(src, dsize)
    
    #cv2.imshow('resized',output) 
    return output

def ResizeWithAspectRatio(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized

def printVideoStats(cap_):
    #print video statistics ; before release of cap ; else no value
    print ("Frame Count   : " + str(cap_.get(7)))  #CV_CAP_PROP_FRAME_COUNT 
    print ("Format        : " + str(cap_.get(8)))  #CV_CAP_PROP_FORMAT)
    print ("Height        : " + str(cap_.get(4)))  #CV_CAP_PROP_FRAME_HEIGHT)
    print ("Width         : " + str(cap_.get(3)))  #CV_CAP_PROP_FRAME_WIDTH)
    print ("Mode          : " + str(cap_.get(9)))  #CV_CAP_PROP_MODE)
    print ("Brightness    : " + str(cap_.get(10))) #CV_CAP_PROP_BRIGHTNESS)
    print ("Fourcc        : " + str(cap_.get(6)))  #CV_CAP_PROP_FOURCC)
    print ("Contrast      : " + str(cap_.get(11))) #CV_CAP_PROP_CONTRAST)
    print ("FramePerSec   : " + str(cap_.get(5)))  #CV_CAP_PROP_FPS)
    
    #Get duration of the video
    FrameCount = cap_.get(7)    #CV_CAP_PROP_FRAME_COUNT 
    FrameperSecond =cap_.get(5) #CV_CAP_PROP_FPS
    DurationSecond = FrameCount/FrameperSecond
    if FrameperSecond>0:
        print ("FrameDuration : " + str(DurationSecond) + "seconds")
 
def putTitle(img, text_x, text_y, text, font_scale = 1, font = cv2.FONT_HERSHEY_SIMPLEX, font_color=(0, 0, 0), text_thickness=1, background_color = (255, 255, 255), background_padding = 5):
    # font_scale = 1.5
    # font = cv2.FONT_HERSHEY_PLAIN

    
    # set the rectangle background to white
    # rectangle_bgr = (255, 255, 255)
    
    # make a black image
    # img = np.zeros((500, 500))
    
    # set some text
    # text = "Some text in a box!"
    
    # get the width and height of the text box
    (text_width, text_height) = cv2.getTextSize(text, font, fontScale=font_scale, thickness=text_thickness)[0]
    
    # set the text start position
    text_offset_x = text_x
    text_offset_y = text_y
    
    # make the coords of the box with a small padding of two pixels
    box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + background_padding*2, text_offset_y - text_height - background_padding*2))
    
    cv2.rectangle(img, box_coords[0], box_coords[1], background_color, cv2.FILLED)
    cv2.putText(img, text, (text_offset_x+background_padding, text_offset_y-background_padding), font, fontScale=font_scale, color=font_color, thickness=text_thickness)


roi_pts = [] # for storing points
roi_scale_percent = 100

def drawROI(img_, points_, border_color_, border_width_, background_color_, backgorund_alpha_):
    mask = np.zeros(img_.shape, np.uint8)
    mask = cv2.polylines(mask, [points_], True, border_color_, border_width_)
    # mask2 = cv2.fillPoly(mask.copy(), [points_], (255, 255, 255)) # for ROI
    mask3 = cv2.fillPoly(mask.copy(), [points_], background_color_) # for displaying images on the desktop
    show_image = cv2.addWeighted(src1=img_, alpha=1, src2=mask3, beta=backgorund_alpha_, gamma=0)
    return show_image

def draw_roi_callback(event, x, y, flags, param):
    #        
    # define mouse callback function
    #

    global roi_pts
    global roi_scale_percent
    global roi_imgRef

    
    img2 = roi_imgRef.copy()
    
    if event == cv2.EVENT_LBUTTONDOWN: # Left click, select point
        roi_pts.append((int(x*(100/roi_scale_percent)), int(y*(100/roi_scale_percent))))  
 
    if event == cv2.EVENT_RBUTTONDOWN: # Right click to cancel the last selected point
        roi_pts.pop()  
 
    if event == cv2.EVENT_MBUTTONDOWN: # 
        mask = np.zeros(roi_imgRef.shape, np.uint8)
        points = np.array(roi_pts, np.int32)
        points = points.reshape((-1, 1, 2))
        mask = cv2.polylines(mask, [points], True, (255, 255, 255), 2)
        mask2 = cv2.fillPoly(mask.copy(), [points], (255, 255, 255)) # for ROI
        mask3 = cv2.fillPoly(mask.copy(), [points], (0, 255, 0)) # for displaying images on the desktop
 
        show_image = cv2.addWeighted(src1=roi_imgRef, alpha=0.8, src2=mask3, beta=0.2, gamma=0)
        
        ROI = cv2.bitwise_and(mask2, roi_imgRef)
        
        rect = cv2.boundingRect(points)

        mask2_cropped      = mask2[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        show_image_cropped = show_image[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        ROI_cropped        = ROI[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        
        
        # cv2.imshow("mask", ResizeWithAspectRatio(mask2_cropped, height = 1000))
        # cv2.imshow("show_img", resizeImage(show_image, roi_scale_percent))        
        cv2.imshow("ROI", ResizeWithAspectRatio(ROI_cropped, height = 1000))
        
        cv2.waitKey(0)

    if len(roi_pts) > 0:
        # Draw the last point in pts
        cv2.circle(img2, roi_pts[-1], 3, (0, 0, 255), -1)
 
    if len(roi_pts) > 1:
        for i in range(len(roi_pts) - 1):
            cv2.circle(img2, roi_pts[i], 5, (0, 0, 255), -1) # x ,y is the coordinates of the mouse click place
            cv2.line(img=img2, pt1=roi_pts[i], pt2=roi_pts[i + 1], color=(255, 0, 0), thickness=2)

    cv2.imshow('image', resizeImage(img2, roi_scale_percent))

def selectROI():
    #
    # select a ROI polygon in an image
    #

    cv2.namedWindow('image')
    cv2.setMouseCallback('image', draw_roi_callback)
    print("[INFO] Click the left button: select the point, right click: delete the last selected point, click the middle button: determine the ROI area")
    print("[INFO] Press ‘S’ to determine the selection area and save it")
    print("[INFO] Press ESC to quit")
    
    while True:
        key = cv2.waitKey(1) & 0xFF
        if key == 27:
            break
        if key == ord("s"):
            saved_data = {
                "ROI": roi_pts
            }
            joblib.dump(value=saved_data, filename="config.pkl")
            print("[INFO] ROI coordinates have been saved to local.")
            break 
        
    cv2.destroyWindow('image')
    
def getRoiframe(img_, points_):
    mask = np.zeros(img_.shape, np.uint8)
    mask = cv2.polylines(mask, [points_], True, (255, 255, 255), 2)
    mask2 = cv2.fillPoly(mask.copy(), [points_], (255, 255, 255)) # for ROI
    
    roi_rect = cv2.boundingRect(points_)
    
    roi_frame = cv2.bitwise_and(mask2, img_)
    roi_frame_cropped = roi_frame[roi_rect[1]: roi_rect[1] + roi_rect[3], roi_rect[0]: roi_rect[0] + roi_rect[2]]
    return roi_frame_cropped

def getRoiOffset(points_, offset):
    
    pts = []
    
    print(points_)

    # points_ = pts_array
    for p in points_:
        pts.append((p[0][0],p[0][1]))
    print("pts: ")
    print(pts)

    pco = pyclipper.PyclipperOffset()
    pco.AddPath(tuple(pts), pyclipper.JT_ROUND, pyclipper.ET_CLOSEDPOLYGON)
    
    solution = pco.Execute(offset)
    print("solution: ")
    print(solution)
    
    # print(pts)
    pts_array = np.array(solution, np.int32).reshape((-1, 1, 2))

    return pts_array

def readImage(filename):
    img = cv2.imread(filename, 0)
    if img is None:
        print('Invalid image:' + filename)
        return None
    else:
        print('Image successfully read...')
        return img



def drawMatches(img1, kp1, img2, kp2, matches, inliers = None):
    """Draw matches and optionally a set of inliers in a different color for debug purposes
    """

    # Create a new output image that concatenates the two images together
    rows1 = img1.shape[0]
    cols1 = img1.shape[1]
    rows2 = img2.shape[0]
    cols2 = img2.shape[1]

    out = np.zeros((max([rows1,rows2]),cols1+cols2,3), dtype='uint8')

    # Place the first image to the left
    out[:rows1,:cols1,:] = np.dstack([img1, img1, img1])

    # Place the next image to the right of it
    out[:rows2,cols1:cols1+cols2,:] = np.dstack([img2, img2, img2])

    # For each pair of points we have between both images
    # draw circles, then connect a line between them
    for mat in matches:

        # Get the matching keypoints for each of the images
        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx

        # x - columns, y - rows
        (x1,y1) = kp1[img1_idx].pt
        (x2,y2) = kp2[img2_idx].pt

        inlier = False

        if inliers is not None:
            for i in inliers:
                if i.item(0) == x1 and i.item(1) == y1 and i.item(2) == x2 and i.item(3) == y2:
                    inlier = True

        # Draw a small circle at both co-ordinates
        cv2.circle(out, (int(x1),int(y1)), 4, (255, 0, 0), 1)
        cv2.circle(out, (int(x2)+cols1,int(y2)), 4, (255, 0, 0), 1)

        # Draw a line in between the two points, draw inliers if we have them
        if inliers is not None and inlier:
            cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), (0, 255, 0), 1)
        elif inliers is not None:
            cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), (0, 0, 0), 1)

        if inliers is None:
            cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), (255, 0, 0), 1)

    return out

def findFeatures(img):
    """Run sift algorithm to find features
    """

    print("Finding Features...")
    # sift = cv2.SIFT()
    sift = cv2.xfeatures2d.SIFT_create()
    keypoints, descriptors = sift.detectAndCompute(img, None)

    img = cv2.drawKeypoints(img, keypoints,None)
    # cv2.imwrite('sift_keypoints.png', img)
    cv2.imshow('sift_keypoints', ResizeWithAspectRatio(img, int(1980/2)))

    return keypoints, descriptors


def matchFeatures(kp1, kp2, desc1, desc2, img1, img2):
    """Match features given a list of keypoints, descriptors, and images
    """
    print("Matching Features...")
    matcher = cv2.BFMatcher(cv2.NORM_L2, True)
    matches = matcher.match(desc1, desc2)
    matchImg = drawMatches(img1,kp1,img2,kp2,matches)
    # cv2.imwrite('Matches.png', matchImg)
    cv2.imshow('Matches', ResizeWithAspectRatio(matchImg, int(1980/2)))
    return matches


def calculateHomography(correspondences):
    """Compute a homography matrix from 4-correspondences"""

    #loop through correspondences and create assemble matrix
    aList = []
    for corr in correspondences:
        p1 = np.matrix([corr.item(0), corr.item(1), 1])
        p2 = np.matrix([corr.item(2), corr.item(3), 1])

        a2 = [0, 0, 0, -p2.item(2) * p1.item(0), -p2.item(2) * p1.item(1), -p2.item(2) * p1.item(2),
              p2.item(1) * p1.item(0), p2.item(1) * p1.item(1), p2.item(1) * p1.item(2)]
        a1 = [-p2.item(2) * p1.item(0), -p2.item(2) * p1.item(1), -p2.item(2) * p1.item(2), 0, 0, 0,
              p2.item(0) * p1.item(0), p2.item(0) * p1.item(1), p2.item(0) * p1.item(2)]
        aList.append(a1)
        aList.append(a2)

    matrixA = np.matrix(aList)

    #svd composition
    u, s, v = np.linalg.svd(matrixA)

    #reshape the min singular value into a 3 by 3 matrix
    h = np.reshape(v[8], (3, 3))

    #normalize and now we have h
    h = (1/h.item(8)) * h
    return h


def geometricDistance(correspondence, h):
    """Calculate the geometric distance between estimated points and original points"""

    p1 = np.transpose(np.matrix([correspondence[0].item(0), correspondence[0].item(1), 1]))
    estimatep2 = np.dot(h, p1)
    estimatep2 = (1/estimatep2.item(2))*estimatep2

    p2 = np.transpose(np.matrix([correspondence[0].item(2), correspondence[0].item(3), 1]))
    error = p2 - estimatep2
    return np.linalg.norm(error)


def ransac(corr, thresh, iterations_max = 1000):
    """Runs through ransac algorithm, creating homographies from random correspondences"""
    maxInliers = []
    finalH = None
    iteration=0
    for i in range(iterations_max):
        #find 4 random points to calculate a homography
        corr1 = corr[random.randrange(0, len(corr))]
        corr2 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((corr1, corr2))
        corr3 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr3))
        corr4 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr4))

        #call the homography function on those points
        h = calculateHomography(randomFour)
        inliers = []

        for i in range(len(corr)):
            d = geometricDistance(corr[i], h)
            if d < 5:
                inliers.append(corr[i])

        if len(inliers) > len(maxInliers):
            maxInliers = inliers
            finalH = h
        print ("it[" +str(iteration) + "] - Corr size: " + str(len(corr)) + " NumInliers: " + str(len(inliers)) + " Max inliers: " + str(len(maxInliers)) + "/" + str(int(len(corr)*thresh)))
        iteration = iteration + 1
        if len(maxInliers) > (len(corr)*thresh):
            break
    return finalH, maxInliers

logging.basicConfig( level=logging.DEBUG, format='[%(levelname)s] - %(threadName)-10s : %(message)s')

def ransac_worker(corr, thresh, iterations_max = 1000):
    maxInliers = []
    finalH = None
    iteration=0
    for i in range(iterations_max):
        #find 4 random points to calculate a homography
        corr1 = corr[random.randrange(0, len(corr))]
        corr2 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((corr1, corr2))
        corr3 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr3))
        corr4 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr4))

        #call the homography function on those points
        h = calculateHomography(randomFour)
        inliers = []

        for i in range(len(corr)):
            d = geometricDistance(corr[i], h)
            if d < 5:
                inliers.append(corr[i])

        if len(inliers) > len(maxInliers):
            maxInliers = inliers
            finalH = h
        # logging.debug(randomFour)
        logging.debug("it[" +str(iteration) + "] - Corr size: " + str(len(corr)) + " NumInliers: " + str(len(inliers)) + " Max inliers: " + str(len(maxInliers)) + "/" + str(int(len(corr)*thresh)))
        
        iteration = iteration + 1
        if len(maxInliers) > (len(corr)*thresh):
            break
    return finalH, maxInliers



def putFrameNumberLabel(frame_, frame_n_):
    frame_out_h, frame_out_w, frame_out_ch = frame_.shape
    frames_text = '%s%.f'%('Frame:',frame_n_)
    frames_text_font = cv2.FONT_HERSHEY_SIMPLEX
    frames_text_font_scale = 1
    frames_text_text_thickness = 2
    frames_text_background_padding = 10
    
    (frames_text_width, frames_text_height) = cv2.getTextSize(frames_text, frames_text_font, fontScale=frames_text_font_scale, thickness=frames_text_text_thickness)[0]
    putTitle(frame_, frame_out_w - frames_text_width - frames_text_background_padding*2, 54, frames_text, font_scale = frames_text_font_scale, font = frames_text_font, font_color=(255, 255, 255), text_thickness=frames_text_text_thickness, background_color = (0, 0, 0), background_padding = 10)
    
def putLabelReferenceStatus(frame_, text):
    frame_out_h, frame_out_w, frame_out_ch = frame_.shape
    frames_text = '%s%s'%('Last Ref. Frame:',text)
    frames_text_font = cv2.FONT_HERSHEY_SIMPLEX
    frames_text_font_scale = 1
    frames_text_text_thickness = 2
    frames_text_background_padding = 10
    
    (frames_text_width, frames_text_height) = cv2.getTextSize(frames_text, frames_text_font, fontScale=frames_text_font_scale, thickness=frames_text_text_thickness)[0]
    putTitle(frame_, frame_out_w - frames_text_width - frames_text_background_padding*2, 100, frames_text, font_scale = frames_text_font_scale, font = frames_text_font, font_color=(0, 0, 255), text_thickness=frames_text_text_thickness, background_color = (255, 255, 255), background_padding = 10)
    
def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")
	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)    

info = ""

def autoAdjust_gama(src, ref, step=0.05, err_min=1, max_iterations=10, imShow=False):
        
    global info
    if(imShow):cv2.imshow('src', src)
    if(imShow):cv2.imshow('REF', ref)

    src_corr  = src
    iteration = max_iterations
    dif_mean  = cv2.mean(src)[0] - cv2.mean(ref)[0]
    err       = abs(dif_mean)
    gamma_i   = 1
    direction = 0
    
    if(dif_mean<0)  : direction = -1
    elif(dif_mean>0): direction = 1
    
    while(err>err_min)and(iteration>0):
        
        if(dif_mean<0):
            #src es más oscura que ref
            if(direction==1):
                direction = -1
                step = step / 2
            gamma_i   = gamma_i + step

        elif(dif_mean>0):
            #src es más clara que ref
            if(direction==-1):
                direction = 1
                step = step / 2
                
            gamma_i   = gamma_i - step
            if(gamma_i<=0):gamma_i=step
            
        src_corr  = adjust_gamma(src, gamma=gamma_i)

        frame_mean = cv2.mean(src_corr)[0]
        dif_mean  = frame_mean - cv2.mean(ref)[0]
        err       = abs(dif_mean)
        iteration = iteration - 1
        
        print("it["+str(max_iterations-iteration)+"] - gamma: " + str(round(gamma_i,2)) + "\t| err: " + str( round(err,2)))

        if(imShow):cv2.imshow('src_corr', src_corr)
        if(imShow):cv2.imshow('dif', cv2.absdiff(src_corr, ref))
        
        info = "it["+str(max_iterations-iteration)+"] - gamma: " + str(round(gamma_i,2)) + " | err: " + str( round(err,2)) + " | mean: " + str( round(cv2.mean(src_corr)[0],2))
        
    return src_corr

def Homography(img, prev_img):

    if prev_img is None:
        return

    orb = cv2.ORB_create()
    kpt1, des1 = orb.detectAndCompute(prev_img, None)
    kpt2, des2 = orb.detectAndCompute(img, None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key=lambda x: x.distance)

    src_pts = np.float32([kpt1[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)
    dst_pts = np.float32([kpt2[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    return M 