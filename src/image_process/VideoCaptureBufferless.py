# -*- coding: utf-8 -*-
"""
Created on Sat May 30 11:54:34 2020

@author: Miguel
"""
import cv2
import queue
import threading
import time

# bufferless VideoCapture
class VideoCapture:

  def __init__(self, name):
    self.name = name
    self.cap = cv2.VideoCapture(name)
    self.q = queue.Queue()
    self.finish = False
    self.finished = False
    self.t = threading.Thread(target=self._reader)
    self.t.daemon = True
    self.t.start()

  # read frames as soon as they are available, keeping only most recent one
  def _reader(self):
    while True:
      ret, frame = self.cap.read()
      if not ret:
        print("fail: " + self.name)
        self.cap.release()
        self.cap = cv2.VideoCapture(self.name)
        continue
        print("fail")
        break
      if not self.q.empty():
        try:
          self.q.get_nowait()   # discard previous (unprocessed) frame
        except queue.Empty:
          pass
      self.q.put(frame)
      
      if(self.finish):break
    
    print("Thread: VideoCapture finished")
    self.finished = True

  def read(self):
    return True, self.q.get()

  def release(self):
      self.finish=True
      while(self.finished==False):
          time.sleep(1)
      self.cap.release()
      
  def isOpened(self):
      return self.cap.isOpened()
  
  def set(self, prop, value):
      self.cap.set(prop, value)
      
  def get(self, prop):
      return self.cap.get(prop)
      
  def getFPS(self):
      return round(float(self.cap.get(cv2.CAP_PROP_FPS)), 2)
  
  def getCurrentFrame(self):
      return self.cap.get(cv2.CAP_PROP_POS_FRAMES)