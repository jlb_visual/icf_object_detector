# -*- coding: utf-8 -*-
"""
Created on Mon May 18 17:03:11 2020

@author: Miguel

image processing functions for ICF detector

"""

import sys
import cv2
import numpy as np
import time
import joblib
import pyclipper
import imutils 
from imutils.video import FileVideoStream
from imutils.video import FPS
from threading import Thread
import random
#import padtransf
import concurrent.futures
import threading
import logging
import threading
import queue
import math
import os.path


    
def getRoiframe(img_, points_):
    mask = np.zeros(img_.shape, np.uint8)
    mask = cv2.polylines(mask, [points_], True, (255, 255, 255), 2)
    mask2 = cv2.fillPoly(mask.copy(), [points_], (255, 255, 255)) # for ROI
    
    roi_rect = cv2.boundingRect(points_)
    
    roi_frame = cv2.bitwise_and(mask2, img_)
    roi_frame_cropped = roi_frame[roi_rect[1]: roi_rect[1] + roi_rect[3], roi_rect[0]: roi_rect[0] + roi_rect[2]]
    return roi_frame_cropped

"""
Homgraphy calculation
"""

#
# Runs sift algorithm to find features
#
def findFeatures(img):
    print("Finding Features...")
    # sift = cv2.SIFT()
    sift = cv2.xfeatures2d.SIFT_create()
    keypoints, descriptors = sift.detectAndCompute(img, None)

    img = cv2.drawKeypoints(img, keypoints,None)
    # cv2.imwrite('sift_keypoints.png', img)
    cv2.imshow('sift_keypoints', ResizeWithAspectRatio(img, int(1980/2)))

    return keypoints, descriptors

#
# Matches features given a list of keypoints, descriptors, and images
#
def matchFeatures(kp1, kp2, desc1, desc2, img1, img2):
    print("Matching Features...")
    matcher = cv2.BFMatcher(cv2.NORM_L2, True)
    matches = matcher.match(desc1, desc2)
    matchImg = drawMatches(img1,kp1,img2,kp2,matches)
    # cv2.imwrite('Matches.png', matchImg)
    cv2.imshow('Matches', ResizeWithAspectRatio(matchImg, int(1980/2)))
    return matches


#
# Computers a homography from 4-correspondences
#
def calculateHomography(correspondences):
    #loop through correspondences and create assemble matrix
    aList = []
    for corr in correspondences:
        p1 = np.matrix([corr.item(0), corr.item(1), 1])
        p2 = np.matrix([corr.item(2), corr.item(3), 1])

        a2 = [0, 0, 0, -p2.item(2) * p1.item(0), -p2.item(2) * p1.item(1), -p2.item(2) * p1.item(2),
              p2.item(1) * p1.item(0), p2.item(1) * p1.item(1), p2.item(1) * p1.item(2)]
        a1 = [-p2.item(2) * p1.item(0), -p2.item(2) * p1.item(1), -p2.item(2) * p1.item(2), 0, 0, 0,
              p2.item(0) * p1.item(0), p2.item(0) * p1.item(1), p2.item(0) * p1.item(2)]
        aList.append(a1)
        aList.append(a2)

    matrixA = np.matrix(aList)

    #svd composition
    u, s, v = np.linalg.svd(matrixA)

    #reshape the min singular value into a 3 by 3 matrix
    h = np.reshape(v[8], (3, 3))

    #normalize and now we have h
    h = (1/h.item(8)) * h
    return h
#
# Calculate the geometric distance between estimated points and original points
#
def geometricDistance(correspondence, h):

    p1 = np.transpose(np.matrix([correspondence[0].item(0), correspondence[0].item(1), 1]))
    estimatep2 = np.dot(h, p1)
    estimatep2 = (1/estimatep2.item(2))*estimatep2

    p2 = np.transpose(np.matrix([correspondence[0].item(2), correspondence[0].item(3), 1]))
    error = p2 - estimatep2
    return np.linalg.norm(error)


#
#Runs through ransac algorithm, creating homographies from random correspondences
#
def ransac(corr, thresh, iterations_max = 1000):
    maxInliers = []
    finalH = None
    iteration=0
    for i in range(iterations_max):
        #find 4 random points to calculate a homography
        corr1 = corr[random.randrange(0, len(corr))]
        corr2 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((corr1, corr2))
        corr3 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr3))
        corr4 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr4))

        #call the homography function on those points
        h = calculateHomography(randomFour)
        inliers = []

        for i in range(len(corr)):
            d = geometricDistance(corr[i], h)
            if d < 5:
                inliers.append(corr[i])

        if len(inliers) > len(maxInliers):
            maxInliers = inliers
            finalH = h
        print ("it[" +str(iteration) + "] - Corr size: " + str(len(corr)) + " NumInliers: " + str(len(inliers)) + " Max inliers: " + str(len(maxInliers)) + "/" + str(int(len(corr)*thresh)))
        iteration = iteration + 1
        if len(maxInliers) > (len(corr)*thresh):
            break
    return finalH, maxInliers

def ransac_worker(corr, thresh, iterations_max = 1000):
    maxInliers = []
    finalH = None
    iteration=0
    for i in range(iterations_max):
        #find 4 random points to calculate a homography
        corr1 = corr[random.randrange(0, len(corr))]
        corr2 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((corr1, corr2))
        corr3 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr3))
        corr4 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr4))

        #call the homography function on those points
        h = calculateHomography(randomFour)
        inliers = []

        for i in range(len(corr)):
            d = geometricDistance(corr[i], h)
            if d < 5:
                inliers.append(corr[i])

        if len(inliers) > len(maxInliers):
            maxInliers = inliers
            finalH = h
        # logging.debug(randomFour)
        logging.debug("it[" +str(iteration) + "] - Corr size: " + str(len(corr)) + " NumInliers: " + str(len(inliers)) + " Max inliers: " + str(len(maxInliers)) + "/" + str(int(len(corr)*thresh)))
        
        iteration = iteration + 1
        if len(maxInliers) > (len(corr)*thresh):
            break
    return finalH, maxInliers

def Homography(img, prev_img):

    if prev_img is None:
        return

    orb = cv2.ORB_create()
    kpt1, des1 = orb.detectAndCompute(prev_img, None)
    kpt2, des2 = orb.detectAndCompute(img, None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key=lambda x: x.distance)

    src_pts = np.float32([kpt1[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)
    dst_pts = np.float32([kpt2[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    return M 


def calculate_matrix_from_rois(img0_roi, roi_pts0, img1_roi, roi_pts1, display = True):

    print("[INFO] calculating matrix")

    img1 = cv2.cvtColor(img0_roi,cv2.COLOR_BGR2GRAY)
    img2 = cv2.cvtColor(img1_roi,cv2.COLOR_BGR2GRAY)

    sift = cv2.xfeatures2d.SIFT_create()

    print("[INFO] sift performed")

    # find key points
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

    if display:
        # show keypoints
        img1_keypoints = cv2.drawKeypoints(img1, kp1,None)
        img2_keypoints = cv2.drawKeypoints(img2, kp2,None)


        cv2.imshow('img1_keypoints', imutils.resize(img1_keypoints, width = int(1980/3)))
        cv2.imshow('img1_keypoints', imutils.resize(img2_keypoints, width = int(1980/3)))

    print("[INFO] Detected and computed. Matching...")

    match = cv2.BFMatcher()
    matches = match.knnMatch(des1,des2,k=2)

    print("[INFO] Matching done")


    good = []
    good_it = 0
    good_step = 0.05
    good_coef = 0
    estimationCoef = 0
    estimationCoef_step = 0.05

    homography_estimation_thresh = 0.8
    RANSAC_REPROJ_THRESHOLD = 5.0
    
    img0_roi_points = np.array(roi_pts0, np.int32).reshape((-1, 1, 2))

    MIN_MATCH_COUNT = int((homography_estimation_thresh - estimationCoef) *len(matches))

    while len(good) < MIN_MATCH_COUNT:
        
        good = []
        good_it = 0
        
        MIN_MATCH_COUNT = int((homography_estimation_thresh - estimationCoef) *len(matches))
        print("total matches: " + str(len(matches)) + " - target: " + str(MIN_MATCH_COUNT))
        
        while len(good) < MIN_MATCH_COUNT:
            
            good_coef = good_step*(good_it+1)
            if(good_coef>=1):
                estimationCoef = estimationCoef + estimationCoef_step
                break
            
            for m,n in matches:
                if m.distance < good_coef*n.distance:
                    good.append(m)
            good_it = good_it + 1
        
            print("it[" + str(good_it) +"] - good_coef[" + str(good_coef) + "] - good matches: " + str(len(good)) + "/" + str(MIN_MATCH_COUNT))
           
            if(len(good) < MIN_MATCH_COUNT): good = []
            
            
    draw_params = dict(matchColor=(0,255,0), singlePointColor=None, flags=2)

    # draw and display matches

    img3 = cv2.drawMatches(img0_roi,kp1,img1_roi,kp2,good,None,**draw_params)

    if display:
        cv2.imshow("matches", imutils.resize(img3, width = 1200))
        cv2.waitKey(0)

    if len(good) >= MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, RANSAC_REPROJ_THRESHOLD)

        h,w = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts, M)
        img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)
        #cv2.imshow("original_image_overlapping.jpg", img2)
    else:
        print("[ERROR] Not enought matches are found - "+str(len(good))+"/"+str(MIN_MATCH_COUNT))

        return None, None, "Not enought matches are found"

        # cv2.destroyAllWindows()
        # sys.exit()


    print("Final homography:\n" + str(M))

    return M, img3, "Homography calculated successfully"
