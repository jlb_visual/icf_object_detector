# -*- coding: utf-8 -*-
"""
Created on Mon May 18 17:03:11 2020

@author: Miguel

image processing functions for ICF detector

"""

import cv2
import numpy as np
import time
import joblib
import json
import pyclipper
import imutils 
from imutils.video import FileVideoStream
from imutils.video import FPS
from threading import Thread
import random


def nothing(x):
    pass

def todict(obj):
    """ Return the object's dict excluding private attributes
    """
    excl = ('_sa_adapter', '_sa_instance_state')
    return {k: v for k, v in vars(obj).items() if not k.startswith('_') and
            not any(hasattr(v, a) for a in excl)}

class ProcParams:
    """ A class that includes all processing parameters.
    If no parameter is passed, default values are used

    For certain parameters (gauss_w_h and kernel), the display values 
    in interface are different than those used in the algorithm, 
    thius two values are stored
    """
    def __init__(self, filename="?", gauss_w_h_display_value=0, gauss_sigma=0, bin_thr=15,
        kernel_display_value=5, it_close0=0, it_open=2, it_close1=10, min_area=3500, max_area=500000):
        self.filename = filename
        self.gauss_w_h_display_value = gauss_w_h_display_value
        self.gauss_sigma = gauss_sigma
        self.bin_thr = bin_thr
        self.kernel_display_value = kernel_display_value
        self.it_close0 = it_close0
        self.it_open = it_open
        self.it_close1 = it_close1
        self.min_area = min_area
        self.max_area = max_area


    def initialize_default(self):
        """
        Initialize with the following default values:

        -gwh 0\
        -gs 0\
        -b 15\
        -k 5\
        -ic0 0\
        -io 2\
        -ic1 10\
        -minA 3500\
        -maxA 500000\
        """
        self.filename = "default"
        self.gauss_w_h_display_value = 2
        self.gauss_sigma = 0
        self.bin_thr = 15
        self.kernel_display_value = 5
        self.it_close0 = 0
        self.it_open = 2
        self.it_close1 = 10
        self.min_area = 3500
        self.max_area = 500000

    def toJSON(self):
        return json.dumps(self.__dict__)

    def write_to_json_file(self, file_name):
        with open(file_name, "w") as file:
            json.dump(self.toJSON(), file,  sort_keys=True, indent=4)

    def class_from_dict(self, dictionary):
        """Constructor"""
        for key in dictionary:
            if key != "filename":
                setattr(self, key, int(dictionary[key]))
            else:
                setattr(self, key, dictionary[key])
    def read_from_json_file(self, file_name):
        with open(file_name) as file:
            pp_dict = json.load(file) 
            self.class_from_dict(pp_dict)
            print(self)

    def mydict(self):
        d = {}
        for  attr_name in vars(self):
            attr_value = getattr(self, attr_name)
            d[attr_name] = attr_value
        return(d)
        return(json.dumps(d))

    def __repr__(self):
        params = ',\n '.join(f'{k}={v}' for k, v in todict(self).items())
        return f"{self.__class__.__name__}({params})"

    
def getRoiframe(img_, points_):
    mask = np.zeros(img_.shape, np.uint8)
    mask = cv2.polylines(mask, [points_], True, (255, 255, 255), 2)
    mask2 = cv2.fillPoly(mask.copy(), [points_], (255, 255, 255)) # for ROI
    
    roi_rect = cv2.boundingRect(points_)
    
    roi_frame = cv2.bitwise_and(mask2, img_)
    roi_frame_cropped = roi_frame[roi_rect[1]: roi_rect[1] + roi_rect[3], roi_rect[0]: roi_rect[0] + roi_rect[2]]
    return roi_frame_cropped

def detect_object(img0_raw, 
    img1_raw, 
    roi_pts0, 
    roi_pts1, 
    H_matrix,
    proc_params,
    debug=False) :

    """
    Main object detection algorithm for ICF.
    
    The idea of the algorithm is that the captures of two cameras of a flat surface
    must be the same if they are geometrically transformed to match one against each other

    For each camera image a polygon has been defined that are approximately matched 

    Input params:

    img0_raw, img1_raw : images to compare
    roi_pts0, roi_pts1 : polygons in each camera that should match
    H_matrix : homography matrix that would make the crops match
    proc_params : image processing parameters used to compare the images
    
    """

    if debug:
        print("Processing detection",
            #"roi_pts0", roi_pts0,
            #"roi_pts1", roi_pts1,
            "proc.params:", proc_params)

    # get crops of the input images 
    img0_roi = getRoiframe(img0_raw, roi_pts0)
    img1_roi = getRoiframe(img1_raw, roi_pts1)
    
    # convert images to bw
    img0_roi_gray = cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY)
    img1_roi_gray = cv2.cvtColor(img1_roi, cv2.COLOR_BGR2GRAY)
    

    src = img1_roi
    dst = img0_roi
    #transf = finalH

    
    crop_warped = cv2.warpPerspective(src, H_matrix, (dst.shape[1], 
        dst.shape[0]), flags=cv2.WARP_INVERSE_MAP)

    img1_roi_mask_warped_retvalbin, img1_roi_mask_warped = \
        cv2.threshold(cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)

    img0_roi_mask_crop_retvalbin, img0_roi_mask_crop = \
        cv2.threshold(cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)

    mask_intersection = cv2.bitwise_and(img0_roi_mask_crop, img1_roi_mask_warped)


    if False:
        cv2.imshow("img1", img0_roi_mask_crop)
        cv2.imshow("img2", img1_roi_mask_warped)
        cv2.imshow("INTERSECTION", mask_intersection)
        cv2.waitKey(0)


    # generar imagen superpuesta
    # se usa solo para presentar resultados
    alpha = 0.5
    beta = 1 - alpha
    blended = cv2.addWeighted(crop_warped, alpha, dst, beta, 1.0)

    # cv2.imshow("blended", blended)
    # de aqui en adelante se trabaja comparando img0_roi y crop_warped (convertida de img1)

    # conversion a BW
    result_img0_input = cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY)
    result_img1_input = cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY)
    
    result_img0 = result_img0_input.copy()
    result_img1 = result_img1_input.copy()
    
    # recortamos el area de interes 
    result_img0 = cv2.bitwise_and(result_img0, mask_intersection)
    result_img1 = cv2.bitwise_and(result_img1, mask_intersection)
                
    #if(histogramEqu_en==1): result_img0 = cv2.equalizeHist(result_img0)
    #if(histogramEqu_en==1): result_img1 = cv2.equalizeHist(result_img1)

    # gaussian blur with the given params
    # build gauss_w_h from display value

    gauss_w_h = 2*proc_params.gauss_w_h_display_value + 1

    result_img0_gaussian = cv2.GaussianBlur(result_img0, (gauss_w_h,
        gauss_w_h), proc_params.gauss_sigma)
    result_img1_gaussian = cv2.GaussianBlur(result_img1, (gauss_w_h,
        gauss_w_h), proc_params.gauss_sigma)

    """
    Esto ya no se usa, se deja como referencia
    max_iterations_ = 50
    result_img1 = autoAdjust_gama(result_img1.copy(), result_img0.copy(), step=0.1, err_min=0.1, max_iterations=max_iterations_)
    result_img1_gaussian = autoAdjust_gama(result_img1_gaussian.copy(), result_img0_gaussian.copy(), step=0.1, err_min=0.1, max_iterations=max_iterations_)
    """
    
    imgCmp = cv2.absdiff(result_img0, result_img1)
    
    # cv2.imshow("imgCmp", imgCmp)
    # se comparan las imagenes desenfocadas
    imgCmp_gaussian = cv2.absdiff(result_img0_gaussian, result_img1_gaussian)

    if False:
        cv2.imshow("img1", result_img0_gaussian)
        cv2.imshow("img2", result_img1_gaussian)
        cv2.imshow("difference", imgCmp_gaussian)
        cv2.waitKey(0)


    #cv2.imshow('DIF = IMG0 - IMG1', ResizeWithAspectRatio(imgCmp_gaussian, height = 500))
    
    img_retvalbin, img_bins = cv2.threshold(imgCmp_gaussian, proc_params.bin_thr, 255, cv2.THRESH_BINARY)


    #cv2.imshow("img_bins", img_bins)

    #build kernel based on proc_params data

    kernel = np.ones((proc_params.kernel_display_value, proc_params.kernel_display_value),np.uint8)

    closing0 = cv2.morphologyEx(img_bins, cv2.MORPH_CLOSE, kernel, 
        iterations = proc_params.it_close0)

    opening = cv2.morphologyEx(closing0, cv2.MORPH_OPEN, kernel, 
        iterations = proc_params.it_open)
    
    closing1 = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel, 
        iterations = proc_params.it_close1)
    
    bins = closing1.copy()

    bin_out = cv2.cvtColor(bins.copy(), cv2.COLOR_GRAY2BGR)

    (contours, hierarchy) = cv2.findContours(bins, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    something_detected = False
    for i in range(len(contours)):  # cycles through all contours in current frame
        if hierarchy[0, i, 3] == -1:  # using hierarchy to only count parent contours (contours not within others)
            area = cv2.contourArea(contours[i])  # area of contour
            if proc_params.min_area < area < proc_params.max_area:  # area threshold for contour
                # print(str(min_area) + "<" + str(area) + "<" + str(max_area))
                # calculating centroids of contours
                cnt = contours[i]
                M = cv2.moments(cnt)
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                # gets bounding points of contour to create rectangle
                # x,y is top left corner and w,h is width and height
                x, y, w, h = cv2.boundingRect(cnt)
                
                #Apply traslation
                # x = x + roi_rect[0]
                # y = y + roi_rect[1]
                # cx = cx + roi_rect[0]
                # cy = cy + roi_rect[1]
                # creates a rectangle around contour
                cv2.rectangle(bin_out, (x, y), (x + w, y + h), (0, 0, 255), 2)
                cv2.rectangle(blended, (x, y), (x + w, y + h), (0, 0, 200), 10)
                cv2.drawContours(blended,[contours[i]],0,(0, 255, 0),3)
                # Prints centroid text in order to double check later on
                # cv2.putText(bin_out, str(cx) + "," + str(cy) + "\r[Area:"+str(area)+"]", (cx + 10, cy + 10), cv2.FONT_HERSHEY_SIMPLEX,1, (0, 255, 0), 2)
                
                text = str(cx) + "," + str(cy) + "\n[A:"+str(area)+"]"
                y0, dy = 50, 50
                for i, line in enumerate(text.split('\n')):
                    y = y0 + i*dy
                    cv2.putText(bin_out, line, (cx + 10, cy + 10 + y), cv2.FONT_HERSHEY_SIMPLEX,1, (0, 255, 0), 2)
                
                cv2.drawMarker(bin_out, (cx, cy), (0, 255, 255), cv2.MARKER_CROSS, markerSize=8, thickness=3,line_type=cv2.LINE_8)
                
                something_detected = True
    
    ret_imgCmp = cv2.cvtColor(imgCmp, cv2.COLOR_GRAY2BGR)
    return something_detected, ret_imgCmp, blended, img_bins, result_img0_gaussian, bin_out

def build_output_image(img0, img1, imgCmp, blended, img_bins, closing0, bin_out, show_option):


    img0_small = imutils.resize(img0,width = 600)
    img1_small = imutils.resize(img1,width = 600)

    upper_image = cv2.hconcat((img0_small, img1_small))

    if show_option == "D1":
        lower_image = imutils.resize(blended,width=upper_image.shape[1])  
    else:

        cmp_small = imutils.resize(imgCmp,width = 600)
        blended_small = imutils.resize(blended,width = 600)
        lower_image = cv2.hconcat((cmp_small, blended_small))




    img_0_1 = cv2.vconcat((upper_image, lower_image))

    return img_0_1

def build_composed_results(blended, img_bins, closing0, bin_out):

    img0 = imutils.resize(blended, height=600)
    img_bins = cv2.cvtColor(img_bins, cv2.COLOR_GRAY2BGR)
    img1 = imutils.resize(img_bins,height=600)
    img2 = imutils.resize(closing0,height= 600)
    img3 = imutils.resize(bin_out, height=600)
    img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)
    img_0_1 = cv2.hconcat((img0,img1))
    img_2_3 = cv2.hconcat((img2,img3))

    img_2_3 = imutils.resize(img_2_3, width = img_0_1.shape[1])

    ret_img =  cv2.vconcat((img_0_1, img_2_3))

    return ret_img