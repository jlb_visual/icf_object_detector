# -*- coding: utf-8 -*-
"""
Created on Mon May 18 17:03:11 2020

@author: Miguel

GUI functions for ICF detector

"""

import cv2
import imutils

"""
ROI selection routines

"""

roi_pts = [] # for storing points
roi_scale_percent = 100
roi_imgRef = None

def drawROI(img_, points_, border_color_, border_width_, background_color_, backgorund_alpha_):
    mask = np.zeros(img_.shape, np.uint8)
    mask = cv2.polylines(mask, [points_], True, border_color_, border_width_)
    # mask2 = cv2.fillPoly(mask.copy(), [points_], (255, 255, 255)) # for ROI
    mask3 = cv2.fillPoly(mask.copy(), [points_], background_color_) # for displaying images on the desktop
    show_image = cv2.addWeighted(src1=img_, alpha=1, src2=mask3, beta=backgorund_alpha_, gamma=0)
    return show_image
        
# :mouse callback function
def draw_roi_callback(event, x, y, flags, param):
    
    # cv2.namedWindow('mask',cv2.WINDOW_NORMAL)
    # cv2.namedWindow('show_img',cv2.WINDOW_NORMAL)
    # cv2.namedWindow('ROI',cv2.WINDOW_NORMAL)
    # cv2.namedWindow('image',cv2.WINDOW_NORMAL)

    global roi_pts
    global roi_scale_percent
    global roi_imgRef

    
    img2 = roi_imgRef.copy()
       
    if event == cv2.EVENT_LBUTTONDOWN: # Left click, select point
        roi_pts.append((int(x*(100/roi_scale_percent)), int(y*(100/roi_scale_percent))))  
 
    if event == cv2.EVENT_RBUTTONDOWN: # Right click to cancel the last selected point
        roi_pts.pop()  
 
    if event == cv2.EVENT_MBUTTONDOWN: # 
        mask = np.zeros(roi_imgRef.shape, np.uint8)
        points = np.array(roi_pts, np.int32)
        points = points.reshape((-1, 1, 2))
        mask = cv2.polylines(mask, [points], True, (255, 255, 255), 2)
        mask2 = cv2.fillPoly(mask.copy(), [points], (255, 255, 255)) # for ROI
        mask3 = cv2.fillPoly(mask.copy(), [points], (0, 255, 0)) # for displaying images on the desktop
 
        show_image = cv2.addWeighted(src1=roi_imgRef, alpha=0.8, src2=mask3, beta=0.2, gamma=0)
        
        ROI = cv2.bitwise_and(mask2, roi_imgRef)
        
        rect = cv2.boundingRect(points)

        mask2_cropped      = mask2[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        show_image_cropped = show_image[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        ROI_cropped        = ROI[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        
        cv2.imshow("ROI", imutils.rsize(ROI_cropped, height = 1000))
        
        cv2.waitKey(0)

    if len(roi_pts) > 0:
        # Draw the last point in pts
        cv2.circle(img2, roi_pts[-1], 3, (0, 0, 255), -1)
 
    if len(roi_pts) > 1:
        for i in range(len(roi_pts) - 1):
            cv2.circle(img2, roi_pts[i], 5, (0, 0, 255), -1) # x ,y is the coordinates of the mouse click place
            cv2.line(img=img2, pt1=roi_pts[i], pt2=roi_pts[i + 1], color=(255, 0, 0), thickness=2)

    cv2.imshow('image', imutils.resize(img2,  width = int(img2.shape[1]*roi_scale_percent/100)))

def selectROI(img):
    global roi_pts
    global roi_scale_percent
    global roi_imgRef
    
    roi_imgRef = img


    # initialize ROI
    roi_pts = []
    roi_scale_percent = 50
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', draw_roi_callback)

    print("[INFO] Click the left button: select the point, right click: delete the last selected point, click the middle button: determine the ROI area")
    print("[INFO] Press ‘S’ to determine the selection area and save it")
    print("[INFO] Press ESC to quit")
    
    while True:
        key = cv2.waitKey(1) & 0xFF
        if key == 27:
            cv2.destroyWindow('image')
            exit()

        if key == ord("s"):
            ret_roi_pts = roi_pts
            print("[INFO] ROI POINTS",ret_roi_pts)
            break 
        
    cv2.destroyWindow('image')
    return ret_roi_pts

def display_cropped(roi_pts, img_ref):

    img0_roi_rect = cv2.boundingRect(roi_pts)
    mask = np.zeros(img_ref.shape, np.uint8)
    mask = cv2.polylines(mask, [roi_pts], True, (255, 255, 255), 2)
    roi_mask = cv2.fillPoly(mask.copy(), [roi_pts], (255, 255, 255)) # for ROI
    img0_roi_mask_crop_retvalbin, img0_roi_mask_crop = cv2.threshold(cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)
    img_to_show = imutils.resize(img0_roi, width=800)
    cv2.imshow('img0_roi', img_to_show)


    img0_roi_rect = cv2.boundingRect(img0_roi_points)
    mask = np.zeros(img0_ref.shape, np.uint8)
    mask = cv2.polylines(mask, [img0_roi_points], True, (255, 255, 255), 2)
    img0_roi_mask = cv2.fillPoly(mask.copy(), [img0_roi_points], (255, 255, 255)) # for ROI
    img0_roi_mask_crop_retvalbin, img0_roi_mask_crop = cv2.threshold(cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)
    img_to_show = imutils.resize(img0_roi, width=800)
    cv2.imshow('img0_roi', img_to_show)

 
def putTitle(img, text_x, text_y, text, font_scale = 1, font = cv2.FONT_HERSHEY_SIMPLEX, font_color=(0, 0, 0), text_thickness=1, background_color = (255, 255, 255), background_padding = 5):
    # font_scale = 1.5
    # font = cv2.FONT_HERSHEY_PLAIN

    
    # set the rectangle background to white
    # rectangle_bgr = (255, 255, 255)
    
    # make a black image
    # img = np.zeros((500, 500))
    
    # set some text
    # text = "Some text in a box!"
    
    # get the width and height of the text box
    (text_width, text_height) = cv2.getTextSize(text, font, fontScale=font_scale, thickness=text_thickness)[0]
    
    # set the text start position
    text_offset_x = text_x
    text_offset_y = text_y
    
    # make the coords of the box with a small padding of two pixels
    box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + background_padding*2, text_offset_y - text_height - background_padding*2))
    
    cv2.rectangle(img, box_coords[0], box_coords[1], background_color, cv2.FILLED)
    cv2.putText(img, text, (text_offset_x+background_padding, text_offset_y-background_padding), font, fontScale=font_scale, color=font_color, thickness=text_thickness)


def generate_output_image(img0, 
	img1,
	 ):


            font_scale = 2
            thickness = 3
            """
            putFrameNumberLabel(img0, frameId0, font_scale = font_scale, text_thickness = thickness)
            putFrameNumberLabel(img1, frameId1, font_scale = font_scale, text_thickness = thickness)
            putLabel(img0, 0, 60, text_status, font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = font_scale, thickness = thickness, font_color=(255, 255, 255), background_padding = 10, background_color=status_color)
            putLabel(img1, 0, 60, text_status, font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = font_scale, thickness = thickness, font_color=(255, 255, 255), background_padding = 10, background_color=status_color)
            """
        
            if(cmd_play==False):
                putLabel(img1, 0, 120, "STOP", font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = 2*font_scale, thickness = thickness, font_color=(0,0,0), background_padding = 10, background_color=(0,0,255))

            img0 = resizeImage(img0,resolution_w)
            img1 = resizeImage(img1,resolution_w)

            img_0_1 = cv2.hconcat((img0, img1))
            
            if(cmd_play==False):
                frame_n = frame_n - frames_skip
                if not (text0==""):
                    if(text0_frames_duration_i<text0_frames_duration):
                        (text_width, text_height) = cv2.getTextSize(text0, cv2.FONT_HERSHEY_SIMPLEX, fontScale=5, thickness=2)[0]
                        putLabel(img_0_1, int(img_0_1.shape[1]/2)-int(text_width/2), int(img_0_1.shape[0]/2)-350, text0, font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = 5, thickness = 2, font_color=(0, 0, 0), background_padding = 50, background_color=(255,255,255))
                        text0_frames_duration_i = text0_frames_duration_i +1
                    else:
                        text0_frames_duration_i = 0
                        text0=""

            img0 = resizeImage(img0,resolution_w)
            img1 = resizeImage(img1,resolution_w)

            img_0_1 = cv2.hconcat((img0, img1))
            


            

            if show_option == 1:
                img_to_show = imutils.resize(blended,width=img_0_1.shape[1])
                putLabel(img_to_show, 0, 0, "COMP", font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = int(font_scale/2), thickness = thickness, font_color=(255,255,255), background_padding = 10, background_color=(255,125,125))

            elif show_option == 2:
                img_to_show = imutils.resize(img_bins,width=img_0_1.shape[1])
                img_to_show = cv2.cvtColor(img_to_show, cv2.COLOR_GRAY2BGR)
                putLabel(img_to_show, 0, 0, "DIFF + BIN", font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = int(font_scale/2), thickness = thickness, font_color=(255,255,255), background_padding = 10, background_color=(255,125,125))

            elif show_option == 3:
                img_to_show = imutils.resize(closing0,width=img_0_1.shape[1])
                img_to_show = cv2.cvtColor(img_to_show, cv2.COLOR_GRAY2BGR)
                putLabel(img_to_show, 0, 0, "CLOSING", font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = int(font_scale/2), thickness = thickness, font_color=(255,255,255), background_padding = 10, background_color=(255,125,125))

            elif show_option == 4:
                img_to_show = imutils.resize(bin_out,width=img_0_1.shape[1])