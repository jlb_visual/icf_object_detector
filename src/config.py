
# -*- coding: utf-8 -*-
"""
Created on 16/01/2022

@author: JLB

Datos de configuracion del programa icf_object_detector

Este archivo contiene las variables comunes al detector y la web de verificacion

También las variables que sedeben definir especificamente para cada site SITE_ID, camera0 y camera1

"""
import os 

# Esto debe cambiarse para identificar cada site
"""
IMPORTANTE

la variable SITE_ID no puede contener el caracter "_"
"""

SITE_ID = "prod0"

camera0 = "rtsp://admin:1234abcd@192.168.1.64"
camera1 = "rtsp://admin:clara222@192.168.1.65"
	
OUTPUT_VIDEO_FOLDER_REL = "../prod/media"
OUTPUT_VIDEO_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), OUTPUT_VIDEO_FOLDER_REL))

RESULTS_FOLDER_REL = "../prod/results"
RESULTS_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), RESULTS_FOLDER_REL))

MONITOR_FOLDER_REL = "../prod/monitor"
MONITOR_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), MONITOR_FOLDER_REL))
