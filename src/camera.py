import cv2

class VideoCamera(object):
    def __init__(self, option):
        if option == "1":
            print("setting camera 1")
            self.video = cv2.VideoCapture("rtsp://admin:12345abc@192.168.2.41")
        else:
            print("setting camera 2")
            self.video = cv2.VideoCapture("rtsp://admin:12345abc@192.168.2.42")
    
    def __del__(self):
        self.video.release()
    
    def get_frame(self):
        success, image = self.video.read()
        gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()
