#!/bin/bash
#	Launch detection in production 
# 	Nota: copiar esto a  /usr/bin
#   	TO DO: cambiar test02 a default


source /home/icf/.virtualenvs/icf/bin/activate
echo "Virtualenv  loaded"


cd /home/icf/icf_object_detector

python3 main_detector.py -u \
	-in0 "rtsp://admin:12345abc@192.168.2.41"\
	-in1 "rtsp://admin:12345abc@192.168.2.42"\
	-id test02\
	-ro
