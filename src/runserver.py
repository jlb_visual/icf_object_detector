import os

import cv2

import urllib.request 

from flask import Flask, Response, flash, request, make_response, redirect, url_for, render_template, jsonify

from camera import VideoCamera

from werkzeug.utils import secure_filename

import numpy as np
#from auxfunct.calculate_matrix import getRoiframe
#from auxfunct.auxiliar import putLabel
import imutils
import json
import time 


from config import RESULTS_FOLDER, MONITOR_FOLDER, OUTPUT_VIDEO_FOLDER, SITE_ID

# Nota: test_files es solo para main_detector
CONFIG_FOLDER_REL = "../prod/config"
CONFIG_FOLDER =  os.path.abspath(os.path.join(os.path.dirname(__file__), CONFIG_FOLDER_REL))

from config import camera0, camera1

from image_process.detector import ProcParams, detect_object, build_output_image

from image_process.homography import getRoiframe, calculate_matrix_from_rois

from image_process.VideoCaptureBufferless import VideoCapture 

"""
Programa de interfaz de verificación de la detección de objetos mediante 
la comparación de las imágenes de dos cámaras 

Todos los archivos se guardan en la carpeta OUTPUT FOLDER

El programa guarda:
- un archivo de resultados de texto que indica el inici y el fin de una detección
- un archivo de video de 1 fps  con los resultados de la detección
- dos archivos de video segun sale de la cámara también de 1fps

"""


app = Flask(__name__,
    static_folder='web/static',
    template_folder='web/templates')

#jsglue = JSGlue(app)else:

app.secret_key = "secret key"

"""
HOMOGRAPHY_FOLDER = '/home/jlberzal/media/icf/homography'
OUTPUT_VIDEO_FOLDER = '/home/jlberzal/media/icf/videos'
RESULTS_FOLDER = '/home/jlberzal/media/icf/results'
"""

app.config['CONFIG_FOLDER'] = CONFIG_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_VIDEO_FOLDER
app.config['RESULTS_FOLDER'] = RESULTS_FOLDER 
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

try:
    app.camera1 = VideoCapture(camera0)
    app.camera2 = VideoCapture(camera1)

except:
    print("[WARNING] Error connecting cameras")

def read_geometry_files(site_id=""):
    
    if site_id == "":
        site_id = SITE_ID

    print("[INFO] Reading geometry files for site: %s"%site_id)
    h_file_path = os.path.join(CONFIG_FOLDER, site_id+"_matrix_file.txt")
    roi0_file_path = os.path.join(CONFIG_FOLDER, site_id+"_pts0.txt")
    roi1_file_path =  os.path.join(CONFIG_FOLDER, site_id+"_pts1.txt")

    if os.path.exists(roi0_file_path) and \
        os.path.exists(roi1_file_path) and \
        os.path.exists(h_file_path):
        
        # 
        # if roi and homography files exist, read them
        # 
        print("[INFO] Reading stored roi and homography files")
        roi_pts0 = np.loadtxt(roi0_file_path)
        roi_pts0 = np.array(roi_pts0, np.int32).reshape((-1, 1, 2))
        roi_pts1 = np.loadtxt(roi1_file_path)
        roi_pts1 = np.array(roi_pts1, np.int32).reshape((-1, 1, 2))
        h_matrix = np.loadtxt(h_file_path)
        return roi_pts0, roi_pts1, h_matrix

    else:
        print("[ERROR] Config files notfound:",roi0_file_path)
        return None, None, None

# Se definen aqui los archivos de geometria porque son una constante en el programa
# asi no se esta ccediento continuamente al archivo

roi_pts0, roi_pts1, h_matrix = read_geometry_files()
    
def gen(camera):
    ds_factor=0.6
    while True:

        success, image = camera.read()

        if success:
            image=cv2.resize(image,None,fx=ds_factor,fy=ds_factor,interpolation=cv2.INTER_AREA)
            ret, jpeg = cv2.imencode('.jpg', image)
            frame = jpeg.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        else:
            print("success Flse")

def gen_processed(proc_params, show_option):
    ds_factor=0.6
    while True:
        success1, image1 = app.camera1.read()
        success2, image2 = app.camera2.read()
        if success1 and success2:
        
            something_detected, imgCmp, blended, img_bins, closing0, bin_out =\
                detect_object(image1, image2,
                    roi_pts0, 
                    roi_pts1, 
                    h_matrix,
                    proc_params)

            img_0_1 =build_output_image(image1, image2, imgCmp, blended, img_bins, closing0, bin_out, show_option)

            ret, jpeg = cv2.imencode('.jpg', img_0_1)
            frame = jpeg.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route('/video_feed/<string:show_option>')
@app.route('/video_feed/<string:show_option>/<string:proc_params_file>/ <string:proc_params_str>')
def video_feed(show_option, process_method=None, proc_params_file=None, proc_params_str=None):
    """
    Generate video feed to be displayed in a page

    options are: direct camera capture or generate an image from the cameras

    in the case of  aprocessed images, the method accepst proc_params as a json string r the name of a proce params file.
    """

    if show_option == "1":
        print("[INFO] setting camera 1")
        return Response(gen(app.camera1),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

    elif show_option == "2":
        print("[INFO] setting camera 2")
        return Response(gen(app.camera2),
                    mimetype='multipart/x-mixed-replace; boundary=frame')
    else:
        print("[INFO] setting composed image")

        proc_params = ProcParams()
        if proc_params_file != "*":
            # read from config_id
            params_file_path = os.path.join(CONFIG_FOLDER,proc_params_file)
            
            if os.path.exists(params_file_path):
                print("Reading params")
                proc_params.read_from_json_file(params_file_path)
            else:
                print('[ERROR] Error reading process params')
                return ('[ERROR] Error reading process params:%s',params_file_path)
        else:

            proc_params_dict = json.loads(proc_params_str)
            proc_params.class_from_dict(proc_params_dict)

        return Response(gen_processed(proc_params, show_option),
                    mimetype='multipart/x-mixed-replace; boundary=frame')  

@app.route('/view_live_feed')
def view_live_feed():
    """
    Page for live feed and image processing viewing
    """
    # Check homography files in config folder
   
    if os.path.isdir(app.config['CONFIG_FOLDER']):
        files = [f for f in os.listdir(app.config['CONFIG_FOLDER']) if f.endswith("file.txt")]
    else:
        files = []

    """
    geom_file_options = []
    for f in files:
        geom_file_options.append(f.replace("_matrix_file.txt",""))

    if len(geom_file_options) > 0:
        default_geom_id = geom_file_options[0]

    # do the same for config files, but store all info as JSON strings

    if len(geom_file_options) > 0:
        default_geom_id = geom_file_options[0]
    """

    proc_params_dict_options = []

    if os.path.isdir(app.config['CONFIG_FOLDER']):
        files = [f for f in os.listdir(app.config['CONFIG_FOLDER']) if f.startswith("params")]
        if len(files) == 0:
            return "[ERROR] Debe haber al menos un archivo de configuracion"

        for f in files:
                ff_name = os.path.join(app.config['CONFIG_FOLDER'], f)
                file = open(ff_name)
                pp_dict = json.load(file)
                file.close()
                print("pp_dict:", pp_dict)
                print("type:", type(pp_dict))
                proc_params_dict_options.append(pp_dict)

    print("proc_params_dict_options:", proc_params_dict_options)

    default_proc_params = {
        "id": "DEFAULT",
        "gauss_w_h_display_value":[1,10],
        "gauss_sigma":[1, 10],
        "bin_thr":[15, 60],
        "kernel_display_value":[5, 25],
        "it_close0":[1,10],
        "it_open":[2, 10],
        "it_close1":[10, 40],
        "min_area":[5000, 25000],
        "max_area":[500000, 1500000]
    }

    return render_template('view_live_video.html', 
        default_proc_params = default_proc_params,
        proc_params_dict_options = proc_params_dict_options,
        info="Info from server")


@app.route('/')
@app.route('/index')
@app.route('/select_results_file')
def select_results_file():

    """Select a file from the results folder.

    Files are presented and when the user selects one,
    session is redirected to view results
    """

    app.camera1.release()
    app.camera2.release()

    def file_size(folder, filename):
        file_stats = os.stat(os.path.join(folder, filename))
        return (file_stats.st_size / 1024)

    folder = app.config['RESULTS_FOLDER']
    if os.path.isdir(folder):
        files = [[f, "%.2f kb"%file_size(folder,f)] for f in os.listdir(folder) if f.endswith("txt")]
        files.sort(reverse=True)
    else:
        files = []
    # when user selects a file, save_video_ref_form will collect info required
    # to be stored
    return render_template(
        'select_results_file.html',
        title='Select Results File',
        files=files)

@app.route('/view_results/<string:results_file>')
def view_results(results_file):
    """
    pagina de visualizacion de resultados
    requiere como como parmaetro el nombre del archivo de resultados
    del que se obtienen:
    - video de la camara 1
    - video de la camara 2
    - video de resultados
    """   
    app.camera1.release()
    app.camera2.release()


    # get names

    site_id = results_file.split("_")[0]

    print("site_id:", site_id)

    params_filename = results_file.replace(site_id +"_", "").replace("_results.txt","")
    # print(params_filename)
    params_filename = params_filename[:-16]

    print("params_filename:",  params_filename)

    # Get homography file options from config folder, for geometries, only use identifier

    if os.path.isdir(app.config['CONFIG_FOLDER']):
        files = [f for f in os.listdir(app.config['CONFIG_FOLDER']) if f.endswith("file.txt")]
    else:
        files = []

    # en esta version se usa el mismo id para geometrias y parametros cuando se graba

    geom_file_options = []
    for f in files:
        geom_file_options.append(f.replace("_matrix_file.txt",""))

    # do the same for config files, but store all info as JSON strings

    proc_params_dict_options = []

    if os.path.isdir(app.config['CONFIG_FOLDER']):
        files = [f for f in os.listdir(app.config['CONFIG_FOLDER']) if f.startswith("params")]
        if len(files) == 0:
            return "[ERROR] Debe haber al menos un archivo de configuracion"

        for f in files:
                ff_name = os.path.join(app.config['CONFIG_FOLDER'], f)
                file = open(ff_name)
                pp_dict = json.load(file)
                file.close()
                print("pp_dict:", pp_dict)
                print("type:", type(pp_dict))
                proc_params_dict_options.append(pp_dict)

    # Verify we have the proper config files

    # pp_options = [p['id'] for p in proc_params_dict_options]

    full_results_file_name = os.path.join(app.config['RESULTS_FOLDER'], results_file)

    results_video_file = os.path.join(app.config['OUTPUT_FOLDER'],results_file.replace("txt","mp4"))

    cap=cv2.VideoCapture(results_video_file)

    nframes = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    lines = []
    starts = []
    ends = []

    with open(os.path.join(full_results_file_name)) as f:
        lines = f.readlines()

    print("lines:", lines)

    for line in lines:
        # print(line)
        if "Object" in line:
            starts.append(int(line.replace("[INFO] Object detected frame:","")))
        if "End" in line:
            ends.append(int(line.replace("[INFO] End of detection frame:","")))

    if len(starts) > 0 and len(ends)>0:
        if starts[0] > ends[0]:
            starts = [0] + starts

    ranges = [[starts[i], ends[i]] for i in range(0, min(len(starts),len(ends))) ]

    default_proc_params = {
        "id": "DEFAULT",
        "gauss_w_h_display_value":[1,10],
        "gauss_sigma":[1, 10],
        "bin_thr":[15, 60],
        "kernel_display_value":[5, 25],
        "it_close0":[1,10],
        "it_open":[2, 10],
        "it_close1":[10, 40],
        "min_area":[5000, 25000],
        "max_area":[500000, 1500000]
    }

    return render_template('view_video.html', 
        site_id=site_id,
        default_proc_params = default_proc_params,
        results_file = results_file,
        number_of_frames = nframes-1, 
        frame = 0,
        proc_params_dict_options = proc_params_dict_options,
        ranges= ranges,
        info="Info from server")


@app.route('/test')
def test():
    return render_template('test.html')



"""
Functions to generate images from video for static display
"""

def get_encoded_image_response(file_name, frame_index):
    try:
        cap = cv2.VideoCapture(file_name)
        CURRENT_FRAME_FLAG = cv2.CAP_PROP_POS_FRAMES
        cap.set(CURRENT_FRAME_FLAG, frame_index)
        success, img = cap.read()
        ret, jpeg = cv2.imencode('.jpg', img)
    except:
        image_name = '/home/jlberzal/Projects/egeo/geoview2.3/app/static/images/img_not_available.jpg'
        img = cv2.imread(image_name)
        ret, jpeg = cv2.imencode('.jpg', img)
    
    response = make_response(jpeg.tobytes())
    response.headers['Content-Type'] = 'image/png'
    return response

@app.route('/get_video_image/<int:frame>')
@app.route('/get_video_image/<int:frame>/<string:date>/<string:option>')
def get_video_image(frame=0, date="20211110",option="1"):
    """ Capture image from video file to be displayed"""
    """
    video_folder = os.path.join(app.config['VIDEO_FILE_DIRECTORY'], video.folder)
    full_file_name = os.path.join(video_folder,video.file_name)
    """

    file_dir = os.path.join(app.config['UPLOAD_FOLDER'], date)
    print(file_dir)
    for file in sorted(os.listdir(file_dir)):
        print(file)
        print(option)
        if os.path.splitext(file)[0]==str(option):
            print("ok")
            file_name=os.path.join(file_dir, file)
            break
    #file_name = os.path.join(app.config['UPLOAD_FOLDER'], "earth.mp4")
    print("file_name")
    cap = cv2.VideoCapture(file_name)
    CURRENT_FRAME_FLAG = cv2.CAP_PROP_POS_FRAMES
    cap.set(CURRENT_FRAME_FLAG, frame)
    success, img = cap.read()

    print("dims:",img.shape())
    ret, jpeg = cv2.imencode('.jpg', img)
        
    response = make_response(jpeg.tobytes())
    response.headers['Content-Type'] = 'image/png'
    return response


@app.route('/get_image/<int:frame>/<string:results_file_name>/<string:image_input>/<string:process_method>')
@app.route('/get_image/<int:frame>/<string:results_file_name>/<string:image_input>/<string:process_method>/<string:proc_params_str>/<string:config_id>')
def get_image(frame, results_file_name, image_input, process_method=None, proc_params_str=None, config_id=None, geom_id=None):
    """ Capture image from video file to be displayed
    the results file name is used to build the video file name to be displayed

    """

    def get_frame_from_video_file(video_file_name, frame):
        """
        get a specific frame from a videofile
        returns an image in opencv format
        """

        cap = cv2.VideoCapture(video_file_name)
        print ("reading from", video_file_name)
        CURRENT_FRAME_FLAG = cv2.CAP_PROP_POS_FRAMES
        cap.set(CURRENT_FRAME_FLAG, frame)
        success, img = cap.read()
        if success:
            return img
        return None
    
    # get identifier from results file name

    print("getting image from ",results_file_name," opt:", image_input)

    """
    from this name, get the names of the video files
    """

    full_results_file_name = os.path.join(app.config['OUTPUT_FOLDER'], results_file_name)

    results_video_file = full_results_file_name.replace("txt","mp4")


    site_id = results_file_name.split("_")[0]

    print("site_id:", site_id)

    params_filename = results_file_name.replace(site_id +"_", "").replace("_results.txt","")
    # print(params_filename)
    params_filename = params_filename[:-16]

    video_file_0 = full_results_file_name.replace("_results.txt","_output_video0.mp4")
    video_file_0 = video_file_0.replace(params_filename, "").replace("__","_")
    video_file_1 = full_results_file_name.replace("_results.txt","_output_video1.mp4")
    video_file_1 = video_file_1.replace(params_filename, "").replace("__","_")
    
    ret_img = None

    if image_input == "1":
        ret_img = get_frame_from_video_file(video_file_0, frame)
        print(ret_img)
    elif image_input == "2":
        ret_img = get_frame_from_video_file(video_file_1, frame)
    elif process_method == "R":
        # display the image recorded as processed
        if image_input == "D1" or image_input == "D2": # esto es redundante pero lo dejo por si se necesita mas adelante
            ret_img = get_frame_from_video_file(results_video_file, frame)
    else:
        img0 = get_frame_from_video_file(video_file_0, frame)
        img1 = get_frame_from_video_file(video_file_1, frame)

        if process_method == "F": # for preset files
            print("herE:", site_id)
            roi_pts0, roi_pts1, h_matrix = read_geometry_files(site_id=site_id)
            if roi_pts0 is None:
                print("Error reading geometries")
                return render_template('test.html')

            proc_params = ProcParams()
            params_file_path = os.path.join(CONFIG_FOLDER,config_id+".txt")
            
            if os.path.exists(params_file_path):
                print("Reading params")
                proc_params.read_from_json_file(params_file_path)
            else:
                print('Error reading process params')
                print(params_file_path, "not found")
                return render_template('test.html')


            something_detected, imgCmp, blended, img_bins, closing0, bin_out =\
                detect_object(img0, img1,
                    roi_pts0, 
                    roi_pts1, 
                    h_matrix,
                    proc_params,
                    debug=True)

            ret_img =build_output_image(img0, img1, imgCmp, blended, img_bins, closing0, bin_out, image_input)

        elif process_method == "M": # for preset files
            roi_pts0, roi_pts1, h_matrix = read_geometry_files(site_id)
            if roi_pts0 is None:
                print("Error reading geometries")
                return render_template('test.html')
            
            proc_params_dict = json.loads(proc_params_str)
            proc_params = ProcParams()
            proc_params.class_from_dict(proc_params_dict)
            print(proc_params_dict)

            something_detected, imgCmp, blended, img_bins, closing0, bin_out =\
                detect_object(img0, img1,
                    roi_pts0, 
                    roi_pts1, 
                    h_matrix,
                    proc_params,
                    debug=True)

            ret_img =build_output_image(img0, img1, imgCmp, blended, img_bins, closing0, bin_out, image_input)

        else:
            print("option ", process_method," not considered")

    if ret_img is not None:
        print("Encoding response...")
        ret, jpeg = cv2.imencode('.jpg', ret_img)
            
        response = make_response(jpeg.tobytes())
        response.headers['Content-Type'] = 'image/png'
        return response
    print("ret_img is None!")
    return None

@app.route('/get_video_data', methods=['POST'])
def get_video_data():
    if request.method == 'POST':
        print(request.json)
        date=request.json['date']
        cap = cv2.VideoCapture(os.path.join(app.config['UPLOAD_FOLDER'], str(date), "1.mp4"))
        nframes = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        lines = []
        starts = []
        ends = []
        parameters = {
            "GAUSSW/H": None,
            "GAUSSSIGMA": None,
            "BINARYTHRESHOLD": None,
            "KERNELSIZE": None,
            "ITCLOSE_0": None,
            "ITOPEN": None,
            "ITCLOSE_1": None,
            "MINAREADETECTION": None,
            "MAXAREADETECTION": None
        }

        with open(os.path.join(app.config['UPLOAD_FOLDER'], str(date), "results.txt")) as f:
            lines = f.readlines()

        first = True
        for line in lines:
            a = line.replace(" ", "")
            if len(a.split(':')) > 1:
                if a.split(':')[0] in parameters:
                    parameters[a.split(':')[0]] = int(a.split(':')[1])
            if "Start" in line:
                if not first:
                    starts.append(int(line[6:]))
                first = False
            if "End" in line:
                ends.append(int(line[4:]))
        date_sent={"frames": nframes,"param":parameters,"sts":starts,"ends":ends}
        json_resp=json.dumps(date_sent)
        return json_resp

# Rutinas para para definir las polilineas

def capturar_imagen(camera, filename):
    cap = cv2.VideoCapture(camera)
    success, frame = cap.read()
    cap.release()

    if success:
        cv2.imwrite('web/static/'+filename, frame)
        return filename
    else:
        return None

@app.route('/definir_polilineas')
def definir_polilineas():
    imagen0 = capturar_imagen(camera0, "imagen0.jpg")
    imagen1 = capturar_imagen(camera1, "imagen1.jpg")
    return render_template('definir_polilineas.html', imagen0="imagen0.jpg", imagen1="imagen1.jpg")

@app.route('/guardar_polilineas', methods=['POST'])
def guardar_polilineas():
    polilinea0 = request.form.get('polilinea0')
    polilinea1 = request.form.get('polilinea1')

    print("Polilínea 1 recibida:", polilinea0)
    print("Polilínea 2 recibida:", polilinea1)

    # Convertir las polilíneas de formato JSON a listas de Python
    polilinea0_list = json.loads(polilinea0)
    polilinea1_list = json.loads(polilinea1)

    # Cargar las imágenes originales
    img0_original = cv2.imread('web/static/imagen0.jpg')
    img1_original = cv2.imread('web/static/imagen1.jpg')

    # Obtener las dimensiones originales de las imágenes
    height0, width0 = img0_original.shape[:2]
    height1, width1 = img1_original.shape[:2]

    # Calcular los factores de escala para redimensionar las polilíneas
    scale_factor_x0 = width0 / 900  # Factor de escala en el eje x
    scale_factor_y0 = height0 / 600  # Factor de escala en el eje y

    scale_factor_x1 = width1 / 900  # Factor de escala en el eje x
    scale_factor_y1 = height1 / 600  # Factor de escala en el eje y

    # Redimensionar las polilíneas al tamaño original de las imágenes
    polilinea0_resized = [[int(point[0] * scale_factor_x0), int(point[1] * scale_factor_y0)] for point in polilinea0_list]
    polilinea1_resized = [[int(point[0] * scale_factor_x1), int(point[1] * scale_factor_y1)] for point in polilinea1_list]

    print("Polilínea 1 resized:", polilinea0_resized)
    print("Polilínea 2 resized:", polilinea1_resized)


    # Convertir las polilíneas redimensionadas a formato adecuado para OpenCV
    polilinea0_points = np.array(polilinea0_resized, np.int32).reshape((-1, 1, 2))
    polilinea1_points = np.array(polilinea1_resized, np.int32).reshape((-1, 1, 2))

    # Obtener las regiones de interés de las imágenes originales
    img0_roi = getRoiframe(img0_original, polilinea0_points)
    img1_roi = getRoiframe(img1_original, polilinea1_points)

    # Guardar las regiones de interés
    cv2.imwrite('web/static/img0_roi.jpg', img0_roi)
    cv2.imwrite('web/static/img1_roi.jpg', img1_roi)

    # Supongamos que aquí determinamos si el proceso fue exitoso o no
    success = True  # Cambiar a False si el proceso falla
    message = "Polilíneas recibidas con éxito" if success else "Error al procesar las polilíneas"
    
    transf, img3, msg  = calculate_matrix_from_rois(img0_roi, polilinea0_points, img1_roi, polilinea1_points, display = False)

    cv2.imwrite('web/static/img0_roi.jpg', img3)
    
    print("[INFO] Matrix calculated")
        
    h_file_path = os.path.join(CONFIG_FOLDER, SITE_ID + "_matrix_file.tmp")
    roi0_file_path = os.path.join(CONFIG_FOLDER,SITE_ID + "_pts0.tmp")
    roi1_file_path =  os.path.join(CONFIG_FOLDER,SITE_ID + "_pts1.tmp")

    #
    # save roi and matrix files, this is always done
    # 
    np.savetxt(roi0_file_path, polilinea0_resized)
    np.savetxt(roi1_file_path, polilinea1_resized)
    np.savetxt(h_file_path,transf) 

    proc_params = ProcParams()

    proc_params.initialize_default()

    something_detected, imgCmp, blended, img_bins, closing0, bin_out =\
            detect_object(img0_original, img1_original,
                polilinea0_points,
                polilinea1_points,
                transf,
                proc_params)

    print("[INFO] Detection done")
    img_0_1 =build_output_image(img0_original, img1_original, imgCmp, blended, img_bins, closing0, bin_out, show_option="D1") 

    cv2.imwrite('web/static/img1_roi.jpg', img_0_1 )

    if transf is not None:
        return jsonify({"success": True, "message": message, "redirect": url_for('validar_polilineas')})
    else:
        return jsonify({"success": False, "message": message})


@app.route('/validar_polilineas', methods=['GET', 'POST'])
def validar_polilineas():
    if request.method == 'POST':
        # Si se recibe una solicitud POST, verificar si se confirmaron los resultados
        confirmacion = request.form.get('confirmacion')
        if confirmacion == 'confirmado':
            # Realizar acciones de validación
            # Por ejemplo, almacenar el resultado en la base de datos o realizar algún otro procesamiento
            return 'Resultados validados exitosamente'
        else:
            # El usuario no confirmó los resultados
            return 'Validación cancelada'
    else:
        # Si se recibe una solicitud GET, simplemente renderizar la plantilla
        return render_template('validar_polilineas.html')

@app.route('/save_params', methods=['POST'])
def save_params():

    params_json = request.form.get('params')

    

    # Verificar si se recibieron los parámetros
    if params_json:

            # Averiguar indice de prametros en los archivos almacenados

            # Obtener una lista de todos los archivos en el directorio
            archivos = os.listdir(CONFIG_FOLDER)

            print("archivos:", archivos)

            # Filtrar los archivos que comienzan con 'params_' y terminan con un número
            archivos_params = [archivo.replace(".txt","") for archivo in archivos if archivo.startswith('params_')]
            print("archivos_params:", archivos_params)

            # Si no hay archivos 'params_', el próximo número será 00
            if not archivos_params:
                siguiente_numero = 0
            else:
                # Encontrar el número más alto entre los archivos existentes
                numeros = [int(archivo[7:]) for archivo in archivos_params]
                siguiente_numero = max(numeros) + 1

            print("Here")
            # Formatear el nombre del próximo archivo
            nombre_siguiente_archivo = f'params_{str(siguiente_numero).zfill(2)}.txt'

            # Aquí puedes usar 'nombre_siguiente_archivo' para guardar tu nuevo archivo

            print("El próximo archivo será: %s"%nombre_siguiente_archivo)

            params_dict = json.loads(params_json)
            params_dict['filename'] = nombre_siguiente_archivo
            # Escribir los parámetros en el archivo
            file_name = os.path.join(CONFIG_FOLDER, nombre_siguiente_archivo)

            with open(file_name, "w") as file:
                json.dump(params_dict, file,  sort_keys=True, indent=4)

            message = "Parámetros guardados con exito en \n{}".format(file_name)
            return jsonify({'message': message})

    else:
        return "No se recibieron parámetros", 400



live_detection_path = os.path.join(MONITOR_FOLDER, "monitor.png")

def generate_live_detection():
    while True:
        with open(live_detection_path, 'rb') as file:
            image_data = file.read()
        yield (b'--frame\r\n' b'Content-Type: image/png\r\n\r\n' + image_data + b'\r\n')
        time.sleep(1)

@app.route("/live_detection")
def live_detection():
    return render_template("live_detection.html")

@app.route("/live_detection_feed")
def live_detection_feed():
    return Response(generate_live_detection(), mimetype="multipart/x-mixed-replace; boundary=frame")

if __name__ == '__main__':

    app.run(host='127.0.0.1', port=5000, debug=True )
