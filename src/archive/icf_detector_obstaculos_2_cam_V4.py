# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 20:29:00 2020

@author: Miguel

Version de JLB y AP

Se utilizan 2 archivos de definicion de ROI para cada stream de videoy una 
matriz de homografia

"""

import sys
import cv2
import numpy as np
import argparse
import os.path
import pathlib
import math
import imutils
from datetime import datetime

from VideoCaptureBufferless import VideoCapture 
import ROI_MultyPoly_selector as ROI_MultyPoly_selector
from Auxiliar import printVideoStats, resizeImage, ResizeWithAspectRatio, putFrameNumberLabel, putLabel, create_blankImg

from calculate_matrix import calculate_matrix, getRoiframe

def nothing(x):
    pass

def frameRw(cap, frameId, step):
    print("RW " + str(step) + " frames")
    frame = frameId - step - 1
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame)
    ret, img = cap.read() 
    text = "(<<) RW: "+str(step)+" Frame"
    return ret, img, text

def frameFw(cap, frameId, step):
    print("FW " + str(step) + " frames")
    frame = frameId + step - 1
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame)
    ret, img = cap.read() 
    text = "(>>) FW: "+str(step)+" Frame"
    return ret, img, text

def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
        for i in np.arange(0, 256)]).astype("uint8")
    # apply gamma correction using the lookup table
    return cv2.LUT(image, table)    

def autoAdjust_gama(src, ref, step=0.05, err_min=1, max_iterations=10, imShow=False):
        
    global info
    if(imShow):cv2.imshow('src', src)
    if(imShow):cv2.imshow('REF', ref)

    src_corr  = src
    iteration = max_iterations
    dif_mean  = cv2.mean(src)[0] - cv2.mean(ref)[0]
    err       = abs(dif_mean)
    gamma_i   = 1
    direction = 0
    
    if(dif_mean<0)  : direction = -1
    elif(dif_mean>0): direction = 1
    
    while(err>err_min)and(iteration>0):
        
        if(dif_mean<0):
            #src es más oscura que ref
            if(direction==1):
                direction = -1
                step = step / 2
            gamma_i   = gamma_i + step

        elif(dif_mean>0):
            #src es más clara que ref
            if(direction==-1):
                direction = 1
                step = step / 2
                
            gamma_i   = gamma_i - step
            if(gamma_i<=0):gamma_i=step
            
        src_corr  = adjust_gamma(src, gamma=gamma_i)

        frame_mean = cv2.mean(src_corr)[0]
        dif_mean  = frame_mean - cv2.mean(ref)[0]
        err       = abs(dif_mean)
        iteration = iteration - 1
        
        #print("it["+str(max_iterations-iteration)+"] - gamma: " + str(round(gamma_i,2)) + "\t| err: " + str( round(err,2)))

        if(imShow):cv2.imshow('src_corr', src_corr)
        if(imShow):cv2.imshow('dif', cv2.absdiff(src_corr, ref))
        
        info = "it["+str(max_iterations-iteration)+"] - gamma: " + str(round(gamma_i,2)) + " | err: " + str( round(err,2)) + " | mean: " + str( round(cv2.mean(src_corr)[0],2))
        
    return src_corr

def create_output_files(id_str):
    """
    Create video and text ouput files
    
    This function is called when the program starts and everyday at 00:00
    """
    date_str = datetime.now().strftime('%Y%m%d_%H%M%S_')

    output_video_file0 = os.path.join("data",id_str +  date_str + "output_video0.mp4")
    output_video_file1 = os.path.join("data",id_str +  date_str + "output_video1.mp4")
    results_video_file = os.path.join("data",id_str +  date_str + "results.mp4")
    output_results_file = os.path.join("data",id_str +  date_str +"results.txt")
    
    results_file = open(output_results_file,"w")
    return output_video_file0, output_video_file1, results_video_file, results_file


def printVideoStats(cap_):
    #print video statistics ; before release of cap ; else no value
    print ("Frame Count   : " + str(cap_.get(7)))  #CV_CAP_PROP_FRAME_COUNT 
    print ("Format        : " + str(cap_.get(8)))  #CV_CAP_PROP_FORMAT)
    print ("Height        : " + str(cap_.get(4)))  #CV_CAP_PROP_FRAME_HEIGHT)
    print ("Width         : " + str(cap_.get(3)))  #CV_CAP_PROP_FRAME_WIDTH)
    print ("Mode          : " + str(cap_.get(9)))  #CV_CAP_PROP_MODE)
    print ("Brightness    : " + str(cap_.get(10))) #CV_CAP_PROP_BRIGHTNESS)
    print ("Fourcc        : " + str(cap_.get(6)))  #CV_CAP_PROP_FOURCC)
    print ("Contrast      : " + str(cap_.get(11))) #CV_CAP_PROP_CONTRAST)
    print ("FramePerSec   : " + str(cap_.get(5)))  #CV_CAP_PROP_FPS)
    
    #Get duration of the video
    FrameCount = cap_.get(7)    #CV_CAP_PROP_FRAME_COUNT 
    FrameperSecond =cap_.get(5) #CV_CAP_PROP_FPS
    DurationSecond = FrameCount/FrameperSecond
    if FrameperSecond>0:
        print ("FrameDuration : " + str(DurationSecond) + "seconds")



if __name__ == "__main__":

##############
# INPUT ARGS #
##############################################################################

    ap = argparse.ArgumentParser()
    
    req1 = ap.add_mutually_exclusive_group(required=True)
    #req1.add_argument('-i', '--image_mode', action="store_true", default= False, help='Select input mode: images')
    req1.add_argument('-v', '--video_mode', action="store_true", default= False, help='Select input mode: video')
    req1.add_argument('-u', '--url_mode'  , action="store_true", default= False, help='Select input mode: url (IP camera)')
       
    ap.add_argument("-in0", "--input0", metavar="path", required=True, help="Input 0 path")
    ap.add_argument("-in1", "--input1", metavar="path", required=True, help="Input 1 path")
    
    ap.add_argument("-off0", "--frame_offset0", type=int, required=False, default=0, help="Starting frame for video 0 file")
    ap.add_argument("-off1", "--frame_offset1", type=int, required=False, default=0, help="Starting frame for video 1 file")
    ap.add_argument("-loop", "--loop_frames", type=int, required=False, default=0, help="Number of frames of the loop")


    ap.add_argument("-id", "--identifier", type=str, required=True, help="Test identifier")

    ap.add_argument("-fs", "--frames_skip", type=int, required=False, default=1, help="Frmaes to skip")
    
    ap.add_argument("-r", "--resolution", type=int, choices=range(9,101), metavar="[10-100]",  required=False, default=100, help="Image resolution")
    ap.add_argument("-gwh", "--gauss_w_h", type=int, choices=range(-1,101), metavar="[0-100]",  required=False, default=0, help="gauss width and height")
    ap.add_argument("-gs", "--gauss_sigma", type=int, choices=range(-1,501), metavar="[0-500]",  required=False, default=0, help="gauss sigma")
    ap.add_argument("-b", "--bin_thr", type=int, choices=range(-1,256), metavar="[0-255]",  required=False, default=10, help="Binary threshold")
    ap.add_argument("-k", "--kernel", type=int, choices=range(0,26), metavar="[1-25]",  required=False, default=5, help="Morphology transf. kernel size")
    ap.add_argument("-ic0", "--it_close0", type=int, choices=range(-1,21), metavar="[0-20]",  required=False, default=0, help="Morphology transf. Close 0")
    ap.add_argument("-io", "--it_open", type=int, choices=range(-1,21), metavar="[0-20]",  required=False, default=0, help="Morphology transf. Open")
    ap.add_argument("-ic1", "--it_close1", type=int, choices=range(-1,21), metavar="[0-20]",  required=False, default=0, help="Morphology transf. Close 1")
    ap.add_argument("-minA", "--min_area", type=int, choices=range(99,50001), metavar="[100-50000]",  required=False, default=3000, help="Min area detection")
    ap.add_argument("-maxA", "--max_area", type=int, choices=range(25000-1,500000+1), metavar="[25000-500000]",  required=False, default=100000, help="Max area detection")
       
    
    ap.add_argument("-int", "--interactive", action='store_true', required=False, help="Run iteractively")
    ap.add_argument("-tb", "--track_bars", action='store_true', required=False, help="Show trackbars")
    ap.add_argument("-sRoi", "--save_roi", action='store_true', required=False, help="Save roi points")
    
    args = vars(ap.parse_args())
    
    #inputMode_image = args["image_mode"]
    inputMode_video = args["video_mode"]
    inputMode_url = args["url_mode"]
    
    in0_path  = args["input0"]
    in1_path  = args["input1"]
    video_offset0  = args["frame_offset0"]
    video_offset1  = args["frame_offset1"]
    loop_frames  = args["loop_frames"]
    
    if not os.path.exists("data"):
        os.mkdir("data")

    identifier = args["identifier"]

    H_File_path = os.path.join("data",identifier+"_matrix_file.txt")
    roi0_file_path = os.path.join("data",identifier+"_pts0.txt")
    roi1_file_path =  os.path.join("data",identifier+"_pts1.txt")


    frames_skip = args["frames_skip"]
    arg_resolution_w = args["resolution"] 
    arg_gauss_w_h = args["gauss_w_h"]
    arg_gauss_sigma = args["gauss_sigma"]
    arg_bin_thr = args["bin_thr"]
    arg_kernel = args["kernel"]
    arg_it_close0 = args["it_close0"]
    arg_it_open = args["it_open"]
    arg_it_close1 = args["it_close1"]
    
    arg_minarea = args["min_area"]
    arg_maxarea = args["max_area"]
    roi_save_en = args["save_roi"]
    interactive = args["interactive"]

    if interactive:
        track_bars_en = args["track_bars"]
    else:
        # si no se corre en interactivo, no hay trackbars
        track_bars_en = False

    if(frames_skip<1):frames_skip=1

    roiGui_en = True


    if(track_bars_en):
        print("SHOW TRACKBARS     : ON")
    else:
        print("SHOW TRACKBARS     : OFF")

    if(roi_save_en):
        print("SAVE ROI TO FILE   : " + str(out_roi_file))
    else:
        print("SAVE ROI TO FILE   : NO")
        
    if roiGui_en == False: 
        print("ROI FILE PATH      : " + str(roi_file_path))
        print("ROI PTS            : ")
        print(roi_pts0)
        print(roi_pts1)


    now = datetime.now()
    timestamp = str(now.year)+str(now.month).zfill(2)+str(now.day).zfill(2)+str(now.hour).zfill(2)+str(now.minute).zfill(2)+str(now.second).zfill(2)

    
    cap0 = None
    cap1 = None
    
    fps0 = 0
    fps1 = 0
    

    """
    por ahora esto no está implementado

    if(inputMode_image):
        print("Checkin images")
        if not os.path.exists(in0_path):
            print(str(in0_path) + " -> PATH does not exists")
            sys.exit()
        
        if not os.path.exists(in1_path):
            print(str(in0_path) + " -> PATH does not exists")
            sys.exit()
    """        
    
    if(inputMode_video):
        print("Checkin video files")
        if not os.path.exists(in0_path):
            print(str(in0_path) + " -> PATH does not exists")
            sys.exit()
        
        cap0 = cv2.VideoCapture(in0_path)
        cap0.set(cv2.CAP_PROP_POS_FRAMES, video_offset0)
        fps0 = round(float(cap0.get(cv2.CAP_PROP_FPS)), 2)
        printVideoStats(cap0)
        
        if(fps0<1): 
            print("FPS0 ERROR (fps<1): " + str(fps0))
            sys.exit()
        
        if not os.path.exists(in1_path):
            print(str(in1_path) + " -> PATH does not exists")
            sys.exit()
            
        cap1 = cv2.VideoCapture(in1_path)
        cap1.set(cv2.CAP_PROP_POS_FRAMES, video_offset1)
        fps1 = round(float(cap1.get(cv2.CAP_PROP_FPS)), 2)
        printVideoStats(cap1)
        
        if(fps1<1): 
            print("FPS1 ERROR (fps<1): " + str(fps1))
            sys.exit()

    if(inputMode_url):
        print("CONNECTING URL " + str(in0_path) + " ...")
        cap0 = VideoCapture(in0_path)
        if cap0.isOpened() == False:
            print(in0_path + " -> CONNECTION FAIL")
            sys.exit()

        print("CONNECTING URL " + str(in1_path) + " ...")
        cap1 = VideoCapture(in1_path)
        if cap1.isOpened() == False:
            print(in1_path + " -> CONNECTION FAIL")
            sys.exit()
        fps1 = cap1.getFPS()

        # printVideoStats(cap1)




    """
    while True:
        frameId0 = cap0.get(1) #current frame number
        frameId1 = cap1.get(1) #current frame number
        
        ret0, img0 = cap0.read()
        ret1, img1 = cap1.read()
        
        imshow_0_1 = cv2.hconcat((img0, img1))
        cv2.imshow('VIDEO SYNQ', ResizeWithAspectRatio(imshow_0_1, int(1600)))
        key = cv2.waitKey(1) & 0xFF   

        if key == ord('q') or key == 27:
            cv2.destroyAllWindows()
            sys.exit()
            
        if key == ord('c'):
            cv2.destroyAllWindows()
            break
    """

    #cap0.set(cv2.CAP_PROP_POS_FRAMES, offset0) 

    ret0, img0 = cap0.read()
    ret1, img1 = cap1.read()

    
    fps0 = 0
    fps1 = 0
    
    if os.path.exists(roi0_file_path) and \
        os.path.exists(roi0_file_path) and \
        os.path.exists(H_File_path):
        print("Reading stored values")
        roi_pts0 = np.loadtxt(roi0_file_path)
        roi_pts0 = np.array(roi_pts0, np.int32).reshape((-1, 1, 2))
        roi_pts1 = np.loadtxt(roi1_file_path)
        roi_pts1 = np.array(roi_pts1, np.int32).reshape((-1, 1, 2))
        H_matrix = np.loadtxt(H_File_path)

    else:
        if not interactive:
            # if running in batch points and homography matrix must be present
            print("No points or homograpphy file found")
        roi_pts0, roi_pts1, H_matrix = calculate_matrix(img0, img1)
        np.savetxt(roi0_file_path, roi_pts0)
        np.savetxt(roi1_file_path, roi_pts1)
        np.savetxt(H_File_path,H_matrix) 
        roi_pts0 = np.array(roi_pts0, np.int32).reshape((-1, 1, 2))  
        roi_pts1 = np.array(roi_pts1, np.int32).reshape((-1, 1, 2))


    # if we are fine to go generate output files
    output_video_file0, output_video_file1, results_video_file, results_file = \
        create_output_files(identifier)

    # record date of file creation
    date_of_file_creation = datetime.now().strftime('%Y%m%d')

################
# ARGS SUMMARY #
##############################################################################
    
    print("")
    print("****************")
    print("* ARGS SUMMARY *")
    print("****************")
    print("INPUT 0 PATH       : " + str(in0_path))
    print("INPUT 1 PATH       : " + str(in1_path))
    
    print("FPS 0              : " + str(fps0))
    print("FPS 1              : " + str(fps1))

    print("VIDEO 0 OFFSET     : " + str(video_offset0))
    print("VIDEO 1 OFFSET     : " + str(video_offset1))
    print("LOOP FRAMES        : " + str(loop_frames))
    print("FRAMES SKIP        : " + str(frames_skip))
    
    print("RESOLUTION         : " + str(arg_resolution_w) + " %")

    print("GAUSS W/H          : " + str(arg_gauss_w_h))
    print("GAUSS SIGMA        : " + str(arg_gauss_sigma))
    print("BINARY THRESHOLD   : " + str(arg_bin_thr))
    print("KERNEL SIZE        : " + str(arg_kernel))
    print("IT CLOSE_0         : " + str(arg_it_close0))
    print("IT OPEN            : " + str(arg_it_open))
    print("IT CLOSE_1         : " + str(arg_it_close1))
    
    print("MIN AREA DETECTION : " + str(arg_minarea))
    print("MAX AREA DETECTION : " + str(arg_maxarea) )
    print("HOMOGRAPHY MATRIX  : ")
    print(H_matrix)
    
    #write args summary to output text file 

    results_file.write("****************\n")
    results_file.write("* ARGS SUMMARY *\n")
    results_file.write("****************\n")
    results_file.write("INPUT 0 PATH       : " + str(in0_path) + "\n")
    results_file.write("INPUT 1 PATH       : " + str(in1_path) +"\n")
    
    results_file.write("FPS 0              : " + str(fps0) +"\n")
    results_file.write("FPS 1              : " + str(fps1) +"\n")

    results_file.write("VIDEO 0 OFFSET     : " + str(video_offset0) +"\n")
    results_file.write("VIDEO 1 OFFSET     : " + str(video_offset1) +"\n")
    results_file.write("LOOP FRAMES        : " + str(loop_frames) +"\n")
    results_file.write("FRAMES SKIP        : " + str(frames_skip) +"\n")
    
    results_file.write("RESOLUTION         : " + str(arg_resolution_w) + " %" +"\n")

    results_file.write("GAUSS W/H          : " + str(arg_gauss_w_h) +"\n")
    results_file.write("GAUSS SIGMA        : " + str(arg_gauss_sigma) +"\n")
    results_file.write("BINARY THRESHOLD   : " + str(arg_bin_thr) +"\n")
    results_file.write("KERNEL SIZE        : " + str(arg_kernel) +"\n")
    results_file.write("IT CLOSE_0         : " + str(arg_it_close0) +"\n")
    results_file.write("IT OPEN            : " + str(arg_it_open) +"\n")
    results_file.write("IT CLOSE_1         : " + str(arg_it_close1) +"\n")
    
    results_file.write("MIN AREA DETECTION : " + str(arg_minarea) +"\n")
    results_file.write("MAX AREA DETECTION : " + str(arg_maxarea) +"\n")    
     
################
# VIDEO STREAM #
##############################################################################


    resolution_w_initial = arg_resolution_w
    resolution_w_min = 10
    
    gauss_w_h_initial = arg_gauss_w_h
    gauss_sigma_initial = arg_gauss_sigma
    bin_thr_initial = arg_bin_thr
    it_close0_initial = arg_it_close0
    it_open_initial = arg_it_open
    it_close1_initial = arg_it_close1
    
    cmd_play = True
    minarea = arg_minarea
    maxarea = arg_maxarea
    kernel = np.ones((arg_kernel,arg_kernel),np.uint8)
    frame_written = 99
    results_file.write("Start %d\n"%frame_written)

    if(track_bars_en):
        # TRACKBARS
        # Create a black image, a window
        cv2.namedWindow('Trackbars', cv2.WINDOW_NORMAL)
        cv2.createTrackbar('res.','Trackbars',resolution_w_initial,100,nothing)
        cv2.createTrackbar('gauss_w_h','Trackbars',gauss_w_h_initial,100,nothing)
        cv2.createTrackbar('gauss_sigma','Trackbars',gauss_sigma_initial,500,nothing)
        cv2.createTrackbar('bin_thr','Trackbars',bin_thr_initial,255,nothing)
        cv2.createTrackbar('it_close0','Trackbars',it_close0_initial,20,nothing)
        cv2.createTrackbar('it_open','Trackbars',it_open_initial,20,nothing)
        cv2.createTrackbar('it_close1','Trackbars',it_close1_initial,20,nothing)
        cv2.createTrackbar('min_area','Trackbars',minarea,minarea*5,nothing)
        cv2.createTrackbar('max_area','Trackbars',maxarea,500000,nothing)
        
    frame_n = 0

    resolution_w_prev = 100
    status_color = (0,0,255)
    cmd_play = True
    text0=""
    text1=""
    text0_frames_duration = int(fps0/frames_skip*4)
    text0_frames_duration_i=0
    text_status = ""
    
    show_option = 1

    if not track_bars_en:
        resolution_w = resolution_w_initial 
        gauss_sigma = gauss_sigma_initial
        gauss_w_h_initial = 2*gauss_w_h_initial+1
        gaussianSize_w = gauss_w_h_initial
        gaussianSize_h = gauss_w_h_initial
        bin_thr = bin_thr_initial
        
        it_close0 = it_close0_initial
        it_open = it_open_initial
        it_close1 = it_close1_initial
        min_area = minarea
        max_area = maxarea


    output_fps = 24
    image_size = (img0.shape[1], img0.shape[0])

    fourcc = cv2.VideoWriter_fourcc(*'MP4V')

    out0 = cv2.VideoWriter(output_video_file0, fourcc, output_fps, image_size)
    out1 = cv2.VideoWriter(output_video_file1, fourcc, output_fps, image_size)
    out_results = None

    current = datetime.now().second

    last_detection = False
    frame_written = 0
    while True:
        if(track_bars_en):
            resolution_w = cv2.getTrackbarPos('res.','Trackbars')    
            gauss_sigma = cv2.getTrackbarPos('gauss_sigma','Trackbars')
            gauss_w_h_initial = 2*cv2.getTrackbarPos('gauss_w_h','Trackbars')+1
            gaussianSize_w = gauss_w_h_initial
            gaussianSize_h = gauss_w_h_initial
            bin_thr = cv2.getTrackbarPos('bin_thr','Trackbars')
            
            it_close0 = cv2.getTrackbarPos('it_close0','Trackbars')
            it_open = cv2.getTrackbarPos('it_open','Trackbars')
            it_close1 = cv2.getTrackbarPos('it_close1','Trackbars')
            min_area = cv2.getTrackbarPos('min_area','Trackbars')
            max_area = cv2.getTrackbarPos('max_area','Trackbars')
        
                        
            if(resolution_w<resolution_w_min):
                cv2.setTrackbarPos('res.','Trackbars', resolution_w_min)
                resolution_w=resolution_w_min

        if(cmd_play):
            ret0, img0 = cap0.read()
            ret1, img1 = cap1.read()
        else:
            img0 = img0_raw.copy()
            img1 = img1_raw.copy()

        frame_n = frame_n + 1
        
        # Nota antes se hacia capturando un fotograma de cada n         
        # Ahora lo hacemos una vez cada segundo

        now = datetime.now()
        if now.second != current:

            current = now.second

            # Si han pasado 12 horas, cerrar archivo de video y crear uno nuevo

            if now.hour == 18 and now.minute == 00 and now.second == 0:
                # verificamos la condicion durante 10 segundos por si hubiera posibles demoras en el proceso
                # if now.strftime('%Y%m%d') != date_of_file_creation:
                if True:
                    # change of day: close files and create new ones

                    out0.release()
                    out1.release()
                    out_results.release()
                    out_results = None
                    results_file.close()

                    output_video_file0, output_video_file1, results_video_file, results_file = \
                        create_output_files(identifier)

                    fourcc = cv2.VideoWriter_fourcc(*'MP4V')

                    out0 = cv2.VideoWriter(output_video_file0, fourcc, output_fps, image_size)
                    out1 = cv2.VideoWriter(output_video_file1, fourcc, output_fps, image_size)



            img0_raw = img0.copy()
            img1_raw = img1.copy()
            
            frameId0 = cap0.get(1) #current frame number
            frameId1 = cap1.get(1) #current frame number


            # escribir en archivos de salida

            out0.write(img0)
            out1.write(img1)


            font_scale = 2
            thickness = 3

            putFrameNumberLabel(img0, frameId0, font_scale = font_scale, text_thickness = thickness)
            putFrameNumberLabel(img1, frameId1, font_scale = font_scale, text_thickness = thickness)
            putLabel(img0, 0, 60, text_status, font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = font_scale, thickness = thickness, font_color=(255, 255, 255), background_padding = 10, background_color=status_color)
            putLabel(img1, 0, 60, text_status, font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = font_scale, thickness = thickness, font_color=(255, 255, 255), background_padding = 10, background_color=status_color)

            # cv2.imshow('img0', ResizeWithAspectRatio(img0, int(1980/2)))
            # cv2.imshow('img1', ResizeWithAspectRatio(img1, int(1980/2)))
            
            if(cmd_play==False):
                putLabel(img1, 0, 120, "STOP", font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = 2*font_scale, thickness = thickness, font_color=(0,0,0), background_padding = 10, background_color=(0,0,255))

            img0 = resizeImage(img0,resolution_w)
            img1 = resizeImage(img1,resolution_w)

            img_0_1 = cv2.hconcat((img0, img1))
            
            if(cmd_play==False):
                frame_n = frame_n - frames_skip
                if not (text0==""):
                    if(text0_frames_duration_i<text0_frames_duration):
                        (text_width, text_height) = cv2.getTextSize(text0, cv2.FONT_HERSHEY_SIMPLEX, fontScale=5, thickness=2)[0]
                        putLabel(img_0_1, int(img_0_1.shape[1]/2)-int(text_width/2), int(img_0_1.shape[0]/2)-350, text0, font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = 5, thickness = 2, font_color=(0, 0, 0), background_padding = 50, background_color=(255,255,255))
                        text0_frames_duration_i = text0_frames_duration_i +1
                    else:
                        text0_frames_duration_i = 0
                        text0=""
            
            
            
            # cv2.imshow('img_0_1', ResizeWithAspectRatio(img_0_1, int(1980/2)))
           
            img0_roi = getRoiframe(img0_raw, roi_pts0)
            img1_roi = getRoiframe(img1_raw, roi_pts1)
            
            img0_roi_gray = cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY)
            img1_roi_gray = cv2.cvtColor(img1_roi, cv2.COLOR_BGR2GRAY)
            
            src = img1_roi
            dst = img0_roi
            #transf = finalH
        
            crop_warped = cv2.warpPerspective(src, H_matrix, (dst.shape[1], dst.shape[0]), flags=cv2.WARP_INVERSE_MAP)

            img1_roi_mask_warped_retvalbin, img1_roi_mask_warped = \
                cv2.threshold(cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)

            img0_roi_mask_crop_retvalbin, img0_roi_mask_crop = \
                cv2.threshold(cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)

            mask_intersection = cv2.bitwise_and(img0_roi_mask_crop, img1_roi_mask_warped)

            # generar imagen superpuesta
            alpha = 0.5
            beta = 1 - alpha
            blended = cv2.addWeighted(crop_warped, alpha, dst, beta, 1.0)

            # de aqui en adelante se trabaja comparando img0_roi y crop_warped (convertida de img1)

            # conversion a BW
            result_img0_input = cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY)
            result_img1_input = cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY)
            
            result_img0 = result_img0_input.copy()
            result_img1 = result_img1_input.copy()
            
            # recortamos el area de interes 
            result_img0 = cv2.bitwise_and(result_img0, mask_intersection)
            result_img1 = cv2.bitwise_and(result_img1, mask_intersection)
                        
            #if(histogramEqu_en==1): result_img0 = cv2.equalizeHist(result_img0)
            #if(histogramEqu_en==1): result_img1 = cv2.equalizeHist(result_img1)
        
            # desenfoque gaussiano con parametros leidos del trackbar
            result_img0_gaussian = cv2.GaussianBlur(result_img0, (gaussianSize_w,gaussianSize_h), gauss_sigma)
            result_img1_gaussian = cv2.GaussianBlur(result_img1, (gaussianSize_w,gaussianSize_h), gauss_sigma)   

            """
            Esto ya no se usa, se deja como referencia
            max_iterations_ = 50
            result_img1 = autoAdjust_gama(result_img1.copy(), result_img0.copy(), step=0.1, err_min=0.1, max_iterations=max_iterations_)
            result_img1_gaussian = autoAdjust_gama(result_img1_gaussian.copy(), result_img0_gaussian.copy(), step=0.1, err_min=0.1, max_iterations=max_iterations_)
            """
            
            imgCmp = cv2.absdiff(result_img0, result_img1)
            
            # se comparan las imagenes desenfocadas
            imgCmp_gaussian = cv2.absdiff(result_img0_gaussian, result_img1_gaussian)
            #cv2.imshow('DIF = IMG0 - IMG1', ResizeWithAspectRatio(imgCmp_gaussian, height = 500))
            
            img_retvalbin, img_bins = cv2.threshold(imgCmp_gaussian, bin_thr, 255, cv2.THRESH_BINARY)

            closing0 = cv2.morphologyEx(img_bins, cv2.MORPH_CLOSE, kernel, iterations = it_close0)

            opening = cv2.morphologyEx(closing0, cv2.MORPH_OPEN, kernel, iterations = it_open)
            
            closing1 = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel, iterations = it_close1)
            
            bins = closing1.copy()
            bin_out = cv2.cvtColor(bins.copy(), cv2.COLOR_GRAY2BGR)
            
    
            (contours, hierarchy) = cv2.findContours(bins, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            something_detected = False
            for i in range(len(contours)):  # cycles through all contours in current frame
                if hierarchy[0, i, 3] == -1:  # using hierarchy to only count parent contours (contours not within others)
                    area = cv2.contourArea(contours[i])  # area of contour
                    if min_area < area < max_area:  # area threshold for contour
                        # print(str(min_area) + "<" + str(area) + "<" + str(max_area))
                        # calculating centroids of contours
                        cnt = contours[i]
                        M = cv2.moments(cnt)
                        cx = int(M['m10'] / M['m00'])
                        cy = int(M['m01'] / M['m00'])
                        # gets bounding points of contour to create rectangle
                        # x,y is top left corner and w,h is width and height
                        x, y, w, h = cv2.boundingRect(cnt)
                        
                        #Apply traslation
                        # x = x + roi_rect[0]
                        # y = y + roi_rect[1]
                        # cx = cx + roi_rect[0]
                        # cy = cy + roi_rect[1]
                        # creates a rectangle around contour
                        cv2.rectangle(bin_out, (x, y), (x + w, y + h), (0, 0, 255), 2)
                        # Prints centroid text in order to double check later on
                        # cv2.putText(bin_out, str(cx) + "," + str(cy) + "\r[Area:"+str(area)+"]", (cx + 10, cy + 10), cv2.FONT_HERSHEY_SIMPLEX,1, (0, 255, 0), 2)
                        
                        text = str(cx) + "," + str(cy) + "\n[A:"+str(area)+"]"
                        y0, dy = 50, 50
                        for i, line in enumerate(text.split('\n')):
                            y = y0 + i*dy
                            cv2.putText(bin_out, line, (cx + 10, cy + 10 + y), cv2.FONT_HERSHEY_SIMPLEX,1, (0, 255, 0), 2)
                        
                        cv2.drawMarker(bin_out, (cx, cy), (0, 255, 255), cv2.MARKER_CROSS, markerSize=8, thickness=3,line_type=cv2.LINE_8)
                        
                        something_detected = True
            
            if(something_detected):
                # print("DETECTED FRAME["+str(frameId0)+"|"+str(frameId1)+"]")
                status_color = (0,0,255)
                text_status = "OBJECT DETECTED!!"
                if last_detection != something_detected:
                    results_file.write("Start %d\n"%frame_written)
            else:
                status_color = (0,255,0)
                text_status = "NO OBJECT DETECTED"
                if last_detection != something_detected:
                    results_file.write("End %d\n"%frame_written)

            frame_written += 1

            last_detection = something_detected

            if show_option == 1:
                img_to_show = imutils.resize(blended,width=img_0_1.shape[1])
                putLabel(img_to_show, 0, 0, "COMP", font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = int(font_scale/2), thickness = thickness, font_color=(255,255,255), background_padding = 10, background_color=(255,125,125))

            elif show_option == 2:
                img_to_show = imutils.resize(img_bins,width=img_0_1.shape[1])
                img_to_show = cv2.cvtColor(img_to_show, cv2.COLOR_GRAY2BGR)
                putLabel(img_to_show, 0, 0, "DIFF + BIN", font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = int(font_scale/2), thickness = thickness, font_color=(255,255,255), background_padding = 10, background_color=(255,125,125))

            elif show_option == 3:
                img_to_show = imutils.resize(closing0,width=img_0_1.shape[1])
                img_to_show = cv2.cvtColor(img_to_show, cv2.COLOR_GRAY2BGR)
                putLabel(img_to_show, 0, 0, "CLOSING", font = cv2.FONT_HERSHEY_SIMPLEX, font_scale = int(font_scale/2), thickness = thickness, font_color=(255,255,255), background_padding = 10, background_color=(255,125,125))

            elif show_option == 4:
                img_to_show = imutils.resize(bin_out,width=img_0_1.shape[1])
            else:
                img_to_show = None

            if img_to_show is not None:
                img_0_1 = cv2.vconcat((img_0_1, img_to_show))

            if interactive:
                cv2.imshow("images", img_0_1)
            
            # Write to results video

            if out_results is None:
                out_size = (img_0_1.shape[1], img_0_1.shape[0])
                fourcc = cv2.VideoWriter_fourcc(*'MP4V')
                out_results = cv2.VideoWriter(results_video_file, fourcc, output_fps, out_size)


            out_results.write(img_0_1)

            # cv2.imshow('BINARY OUTPUT', ResizeWithAspectRatio(bin_out, height = 500))
            
            #cv2.imshow('BINARY MASK', ResizeWithAspectRatio(test_bins, height = 500))

        if interactive:            
            key = cv2.waitKey(1) & 0xFF

            if key == ord('q'): #exit
                break

            if key == ord('s'): #stop
                cmd_play = not cmd_play

            if key != 255:
                print("chr(key):",chr(key))

            if chr(key) in ['0','1','2','3','4','5']: #change display
                show_option = int(chr(key))
        
        
    cap0.release()
    cap1.release()
    out0.release()
    out1.release()
    out_results.release()
    results_file.close()

    cv2.destroyAllWindows()
    sys.exit()