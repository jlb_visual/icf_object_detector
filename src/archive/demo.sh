if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit
fi

test_name=prueba
if [ $# -eq 2 ]
  then
  	# if 2 arguments are passed, the second must be the test identification
  	# if the test exists, no matrix calculation will be done
  	test_name=$2
fi


matrix_filename=$test_name"_matrix_file.txt"
pts0_filename=$test_name"_pts0.txt"
pts1_filename=$test_name"_pts1.txt"

if [ $1 = "u" ]
  then
  	echo "Url selected"
  	python main_detector.py -u \
										   -in0 "rtsp://admin:camara@193.168.0.152"\
										   -in1 "rtsp://admin:camara@193.168.0.137"\
										   -off0 3518\
										   -off1 9444\
										   -H $matrix_filename\
										   --frames_skip 10\
										   -gwh 0\
										   -gs 0\
										   -b 15\
										   -k 5\
										   -ic0 0\
										   -io 2\
										   -ic1 10\
										   -minA 3500\
										   -maxA 500000\
										   -roi0 $pts0_filename\
										   -roi1 $pts1_filename\
										   -tb
fi

if [ $1 = "f" ]
  then
	python main_detector.py -v \
										   -in0 "videos_test/2020-05-22/cam0/ch01_00000000036000000.mp4"\
										   -in1 "videos_test/2020-05-22/cam1/ch01_00000000061000000.mp4"\
										   -off0 3518\
										   -off1 9444\
										   -H $matrix_filename\
										   --frames_skip 10\
										   -gwh 0\
										   -gs 0\
										   -b 15\
										   -k 5\
										   -ic0 0\
										   -io 2\
										   -ic1 10\
										   -minA 3500\
										   -maxA 500000\
										   -roi0 $pts0_filename\
										   -roi1 $pts1_filename\
										   -tb
fi 										   