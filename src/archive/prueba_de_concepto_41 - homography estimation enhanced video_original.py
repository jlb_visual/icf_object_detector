# -*- coding: utf-8 -*-
"""
Created on Mon May 18 17:03:11 2020

@author: Miguel

#prueba_de_concepto_40 - homography estimation enhanced

"""

import sys
import cv2
import numpy as np
import time
import joblib
import pyclipper
import imutils 
from imutils.video import FileVideoStream
from imutils.video import FPS
from threading import Thread
import random
import padtransf
import concurrent.futures
import threading
import logging
import threading
import queue
import math
import os.path

###############
## FUNCTIONS ##
#################################################################################################################################
def nothing(x):
    pass

def resizeImage(src, scale_percent):
    
    #calculate the percent of original dimensions
    width = int(src.shape[1] * scale_percent / 100)
    height = int(src.shape[0] * scale_percent / 100)
    
    # dsize
    dsize = (width, height)
    
    # resize image
    output = cv2.resize(src, dsize)
    
    #cv2.imshow('resized',output) 
    return output

def ResizeWithAspectRatio(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized

def printVideoStats(cap_):
    #print video statistics ; before release of cap ; else no value
    print ("Frame Count   : " + str(cap_.get(7)))  #CV_CAP_PROP_FRAME_COUNT 
    print ("Format        : " + str(cap_.get(8)))  #CV_CAP_PROP_FORMAT)
    print ("Height        : " + str(cap_.get(4)))  #CV_CAP_PROP_FRAME_HEIGHT)
    print ("Width         : " + str(cap_.get(3)))  #CV_CAP_PROP_FRAME_WIDTH)
    print ("Mode          : " + str(cap_.get(9)))  #CV_CAP_PROP_MODE)
    print ("Brightness    : " + str(cap_.get(10))) #CV_CAP_PROP_BRIGHTNESS)
    print ("Fourcc        : " + str(cap_.get(6)))  #CV_CAP_PROP_FOURCC)
    print ("Contrast      : " + str(cap_.get(11))) #CV_CAP_PROP_CONTRAST)
    print ("FramePerSec   : " + str(cap_.get(5)))  #CV_CAP_PROP_FPS)
    
    #Get duration of the video
    FrameCount = cap_.get(7)    #CV_CAP_PROP_FRAME_COUNT 
    FrameperSecond =cap_.get(5) #CV_CAP_PROP_FPS
    DurationSecond = FrameCount/FrameperSecond
    if FrameperSecond>0:
        print ("FrameDuration : " + str(DurationSecond) + "seconds")
 
def putTitle(img, text_x, text_y, text, font_scale = 1, font = cv2.FONT_HERSHEY_SIMPLEX, font_color=(0, 0, 0), text_thickness=1, background_color = (255, 255, 255), background_padding = 5):
    # font_scale = 1.5
    # font = cv2.FONT_HERSHEY_PLAIN

    
    # set the rectangle background to white
    # rectangle_bgr = (255, 255, 255)
    
    # make a black image
    # img = np.zeros((500, 500))
    
    # set some text
    # text = "Some text in a box!"
    
    # get the width and height of the text box
    (text_width, text_height) = cv2.getTextSize(text, font, fontScale=font_scale, thickness=text_thickness)[0]
    
    # set the text start position
    text_offset_x = text_x
    text_offset_y = text_y
    
    # make the coords of the box with a small padding of two pixels
    box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + background_padding*2, text_offset_y - text_height - background_padding*2))
    
    cv2.rectangle(img, box_coords[0], box_coords[1], background_color, cv2.FILLED)
    cv2.putText(img, text, (text_offset_x+background_padding, text_offset_y-background_padding), font, fontScale=font_scale, color=font_color, thickness=text_thickness)


###################################################################################################################
# ROI SELECTION #
###################################################################################################################
roi_pts = [] # for storing points
roi_scale_percent = 100

def drawROI(img_, points_, border_color_, border_width_, background_color_, backgorund_alpha_):
    mask = np.zeros(img_.shape, np.uint8)
    mask = cv2.polylines(mask, [points_], True, border_color_, border_width_)
    # mask2 = cv2.fillPoly(mask.copy(), [points_], (255, 255, 255)) # for ROI
    mask3 = cv2.fillPoly(mask.copy(), [points_], background_color_) # for displaying images on the desktop
    show_image = cv2.addWeighted(src1=img_, alpha=1, src2=mask3, beta=backgorund_alpha_, gamma=0)
    return show_image
        
# :mouse callback function
def draw_roi_callback(event, x, y, flags, param):
    
    # cv2.namedWindow('mask',cv2.WINDOW_NORMAL)
    # cv2.namedWindow('show_img',cv2.WINDOW_NORMAL)
    # cv2.namedWindow('ROI',cv2.WINDOW_NORMAL)
    # cv2.namedWindow('image',cv2.WINDOW_NORMAL)

    global roi_pts
    global roi_scale_percent
    global roi_imgRef

    
    img2 = roi_imgRef.copy()
    
    if event == cv2.EVENT_LBUTTONDOWN: # Left click, select point
        roi_pts.append((int(x*(100/roi_scale_percent)), int(y*(100/roi_scale_percent))))  
 
    if event == cv2.EVENT_RBUTTONDOWN: # Right click to cancel the last selected point
        roi_pts.pop()  
 
    if event == cv2.EVENT_MBUTTONDOWN: # 
        mask = np.zeros(roi_imgRef.shape, np.uint8)
        points = np.array(roi_pts, np.int32)
        points = points.reshape((-1, 1, 2))
        mask = cv2.polylines(mask, [points], True, (255, 255, 255), 2)
        mask2 = cv2.fillPoly(mask.copy(), [points], (255, 255, 255)) # for ROI
        mask3 = cv2.fillPoly(mask.copy(), [points], (0, 255, 0)) # for displaying images on the desktop
 
        show_image = cv2.addWeighted(src1=roi_imgRef, alpha=0.8, src2=mask3, beta=0.2, gamma=0)
        
        ROI = cv2.bitwise_and(mask2, roi_imgRef)
        
        rect = cv2.boundingRect(points)

        mask2_cropped      = mask2[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        show_image_cropped = show_image[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        ROI_cropped        = ROI[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
        
        
        # cv2.imshow("mask", ResizeWithAspectRatio(mask2_cropped, height = 1000))
        # cv2.imshow("show_img", resizeImage(show_image, roi_scale_percent))        
        cv2.imshow("ROI", ResizeWithAspectRatio(ROI_cropped, height = 1000))
        
        cv2.waitKey(0)

    if len(roi_pts) > 0:
        # Draw the last point in pts
        cv2.circle(img2, roi_pts[-1], 3, (0, 0, 255), -1)
 
    if len(roi_pts) > 1:
        for i in range(len(roi_pts) - 1):
            cv2.circle(img2, roi_pts[i], 5, (0, 0, 255), -1) # x ,y is the coordinates of the mouse click place
            cv2.line(img=img2, pt1=roi_pts[i], pt2=roi_pts[i + 1], color=(255, 0, 0), thickness=2)

    cv2.imshow('image', resizeImage(img2, roi_scale_percent))

def selectROI():
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', draw_roi_callback)
    print("[INFO] Click the left button: select the point, right click: delete the last selected point, click the middle button: determine the ROI area")
    print("[INFO] Press ‘S’ to determine the selection area and save it")
    print("[INFO] Press ESC to quit")
    
    while True:
        key = cv2.waitKey(1) & 0xFF
        if key == 27:
            break
        if key == ord("s"):
            saved_data = {
                "ROI": roi_pts
            }
            joblib.dump(value=saved_data, filename="config.pkl")
            print("[INFO] ROI coordinates have been saved to local.")
            break 
        
    # cv2.destroyAllWindows()
    cv2.destroyWindow('image')
    #cv2.destroyWindow('mask')
    #cv2.destroyWindow('show_img')
    #cv2.destroyWindow('ROI')
    
def getRoiframe(img_, points_):
    mask = np.zeros(img_.shape, np.uint8)
    mask = cv2.polylines(mask, [points_], True, (255, 255, 255), 2)
    mask2 = cv2.fillPoly(mask.copy(), [points_], (255, 255, 255)) # for ROI
    
    roi_rect = cv2.boundingRect(points_)
    
    roi_frame = cv2.bitwise_and(mask2, img_)
    roi_frame_cropped = roi_frame[roi_rect[1]: roi_rect[1] + roi_rect[3], roi_rect[0]: roi_rect[0] + roi_rect[2]]
    return roi_frame_cropped

def getRoiOffset(points_, offset):
    
    pts = []
    
    print(points_)

    # points_ = pts_array
    for p in points_:
        pts.append((p[0][0],p[0][1]))
    print("pts: ")
    print(pts)

    pco = pyclipper.PyclipperOffset()
    pco.AddPath(tuple(pts), pyclipper.JT_ROUND, pyclipper.ET_CLOSEDPOLYGON)
    
    solution = pco.Execute(offset)
    print("solution: ")
    print(solution)
    
    # print(pts)
    pts_array = np.array(solution, np.int32).reshape((-1, 1, 2))

    return pts_array

###################################################################################################################
# HOMOGRAPHY SELECTION #
###################################################################################################################
#
# Read in an image file, errors out if we can't find the file
#
def readImage(filename):
    img = cv2.imread(filename, 0)
    if img is None:
        print('Invalid image:' + filename)
        return None
    else:
        print('Image successfully read...')
        return img


# This draws matches and optionally a set of inliers in a different color
# Note: I lifted this drawing portion from stackoverflow and adjusted it to my needs because OpenCV 2.4.11 does not
# include the drawMatches function
def drawMatches(img1, kp1, img2, kp2, matches, inliers = None):
    # Create a new output image that concatenates the two images together
    rows1 = img1.shape[0]
    cols1 = img1.shape[1]
    rows2 = img2.shape[0]
    cols2 = img2.shape[1]

    out = np.zeros((max([rows1,rows2]),cols1+cols2,3), dtype='uint8')

    # Place the first image to the left
    out[:rows1,:cols1,:] = np.dstack([img1, img1, img1])

    # Place the next image to the right of it
    out[:rows2,cols1:cols1+cols2,:] = np.dstack([img2, img2, img2])

    # For each pair of points we have between both images
    # draw circles, then connect a line between them
    for mat in matches:

        # Get the matching keypoints for each of the images
        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx

        # x - columns, y - rows
        (x1,y1) = kp1[img1_idx].pt
        (x2,y2) = kp2[img2_idx].pt

        inlier = False

        if inliers is not None:
            for i in inliers:
                if i.item(0) == x1 and i.item(1) == y1 and i.item(2) == x2 and i.item(3) == y2:
                    inlier = True

        # Draw a small circle at both co-ordinates
        cv2.circle(out, (int(x1),int(y1)), 4, (255, 0, 0), 1)
        cv2.circle(out, (int(x2)+cols1,int(y2)), 4, (255, 0, 0), 1)

        # Draw a line in between the two points, draw inliers if we have them
        if inliers is not None and inlier:
            cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), (0, 255, 0), 1)
        elif inliers is not None:
            cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), (0, 0, 0), 1)

        if inliers is None:
            cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), (255, 0, 0), 1)

    return out

#
# Runs sift algorithm to find features
#
def findFeatures(img):
    print("Finding Features...")
    # sift = cv2.SIFT()
    sift = cv2.xfeatures2d.SIFT_create()
    keypoints, descriptors = sift.detectAndCompute(img, None)

    img = cv2.drawKeypoints(img, keypoints,None)
    # cv2.imwrite('sift_keypoints.png', img)
    cv2.imshow('sift_keypoints', ResizeWithAspectRatio(img, int(1980/2)))

    return keypoints, descriptors

#
# Matches features given a list of keypoints, descriptors, and images
#
def matchFeatures(kp1, kp2, desc1, desc2, img1, img2):
    print("Matching Features...")
    matcher = cv2.BFMatcher(cv2.NORM_L2, True)
    matches = matcher.match(desc1, desc2)
    matchImg = drawMatches(img1,kp1,img2,kp2,matches)
    # cv2.imwrite('Matches.png', matchImg)
    cv2.imshow('Matches', ResizeWithAspectRatio(matchImg, int(1980/2)))
    return matches


#
# Computers a homography from 4-correspondences
#
def calculateHomography(correspondences):
    #loop through correspondences and create assemble matrix
    aList = []
    for corr in correspondences:
        p1 = np.matrix([corr.item(0), corr.item(1), 1])
        p2 = np.matrix([corr.item(2), corr.item(3), 1])

        a2 = [0, 0, 0, -p2.item(2) * p1.item(0), -p2.item(2) * p1.item(1), -p2.item(2) * p1.item(2),
              p2.item(1) * p1.item(0), p2.item(1) * p1.item(1), p2.item(1) * p1.item(2)]
        a1 = [-p2.item(2) * p1.item(0), -p2.item(2) * p1.item(1), -p2.item(2) * p1.item(2), 0, 0, 0,
              p2.item(0) * p1.item(0), p2.item(0) * p1.item(1), p2.item(0) * p1.item(2)]
        aList.append(a1)
        aList.append(a2)

    matrixA = np.matrix(aList)

    #svd composition
    u, s, v = np.linalg.svd(matrixA)

    #reshape the min singular value into a 3 by 3 matrix
    h = np.reshape(v[8], (3, 3))

    #normalize and now we have h
    h = (1/h.item(8)) * h
    return h


#
#Calculate the geometric distance between estimated points and original points
#
def geometricDistance(correspondence, h):

    p1 = np.transpose(np.matrix([correspondence[0].item(0), correspondence[0].item(1), 1]))
    estimatep2 = np.dot(h, p1)
    estimatep2 = (1/estimatep2.item(2))*estimatep2

    p2 = np.transpose(np.matrix([correspondence[0].item(2), correspondence[0].item(3), 1]))
    error = p2 - estimatep2
    return np.linalg.norm(error)


#
#Runs through ransac algorithm, creating homographies from random correspondences
#

def ransac(corr, thresh, iterations_max = 1000):
    maxInliers = []
    finalH = None
    iteration=0
    for i in range(iterations_max):
        #find 4 random points to calculate a homography
        corr1 = corr[random.randrange(0, len(corr))]
        corr2 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((corr1, corr2))
        corr3 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr3))
        corr4 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr4))

        #call the homography function on those points
        h = calculateHomography(randomFour)
        inliers = []

        for i in range(len(corr)):
            d = geometricDistance(corr[i], h)
            if d < 5:
                inliers.append(corr[i])

        if len(inliers) > len(maxInliers):
            maxInliers = inliers
            finalH = h
        print ("it[" +str(iteration) + "] - Corr size: " + str(len(corr)) + " NumInliers: " + str(len(inliers)) + " Max inliers: " + str(len(maxInliers)) + "/" + str(int(len(corr)*thresh)))
        iteration = iteration + 1
        if len(maxInliers) > (len(corr)*thresh):
            break
    return finalH, maxInliers

logging.basicConfig( level=logging.DEBUG, format='[%(levelname)s] - %(threadName)-10s : %(message)s')
def ransac_worker(corr, thresh, iterations_max = 1000):
    maxInliers = []
    finalH = None
    iteration=0
    for i in range(iterations_max):
        #find 4 random points to calculate a homography
        corr1 = corr[random.randrange(0, len(corr))]
        corr2 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((corr1, corr2))
        corr3 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr3))
        corr4 = corr[random.randrange(0, len(corr))]
        randomFour = np.vstack((randomFour, corr4))

        #call the homography function on those points
        h = calculateHomography(randomFour)
        inliers = []

        for i in range(len(corr)):
            d = geometricDistance(corr[i], h)
            if d < 5:
                inliers.append(corr[i])

        if len(inliers) > len(maxInliers):
            maxInliers = inliers
            finalH = h
        # logging.debug(randomFour)
        logging.debug("it[" +str(iteration) + "] - Corr size: " + str(len(corr)) + " NumInliers: " + str(len(inliers)) + " Max inliers: " + str(len(maxInliers)) + "/" + str(int(len(corr)*thresh)))
        
        iteration = iteration + 1
        if len(maxInliers) > (len(corr)*thresh):
            break
    return finalH, maxInliers

###################################################################################################################
def putFrameNumberLabel(frame_, frame_n_):
    frame_out_h, frame_out_w, frame_out_ch = frame_.shape
    frames_text = '%s%.f'%('Frame:',frame_n_)
    frames_text_font = cv2.FONT_HERSHEY_SIMPLEX
    frames_text_font_scale = 1
    frames_text_text_thickness = 2
    frames_text_background_padding = 10
    
    (frames_text_width, frames_text_height) = cv2.getTextSize(frames_text, frames_text_font, fontScale=frames_text_font_scale, thickness=frames_text_text_thickness)[0]
    putTitle(frame_, frame_out_w - frames_text_width - frames_text_background_padding*2, 54, frames_text, font_scale = frames_text_font_scale, font = frames_text_font, font_color=(255, 255, 255), text_thickness=frames_text_text_thickness, background_color = (0, 0, 0), background_padding = 10)
    
def putLabelReferenceStatus(frame_, text):
    frame_out_h, frame_out_w, frame_out_ch = frame_.shape
    frames_text = '%s%s'%('Last Ref. Frame:',text)
    frames_text_font = cv2.FONT_HERSHEY_SIMPLEX
    frames_text_font_scale = 1
    frames_text_text_thickness = 2
    frames_text_background_padding = 10
    
    (frames_text_width, frames_text_height) = cv2.getTextSize(frames_text, frames_text_font, fontScale=frames_text_font_scale, thickness=frames_text_text_thickness)[0]
    putTitle(frame_, frame_out_w - frames_text_width - frames_text_background_padding*2, 100, frames_text, font_scale = frames_text_font_scale, font = frames_text_font, font_color=(0, 0, 255), text_thickness=frames_text_text_thickness, background_color = (255, 255, 255), background_padding = 10)
    
def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")
	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)    

info = ""

def autoAdjust_gama(src, ref, step=0.05, err_min=1, max_iterations=10, imShow=False):
        
    global info
    if(imShow):cv2.imshow('src', src)
    if(imShow):cv2.imshow('REF', ref)

    src_corr  = src
    iteration = max_iterations
    dif_mean  = cv2.mean(src)[0] - cv2.mean(ref)[0]
    err       = abs(dif_mean)
    gamma_i   = 1
    direction = 0
    
    if(dif_mean<0)  : direction = -1
    elif(dif_mean>0): direction = 1
    
    while(err>err_min)and(iteration>0):
        
        if(dif_mean<0):
            #src es más oscura que ref
            if(direction==1):
                direction = -1
                step = step / 2
            gamma_i   = gamma_i + step

        elif(dif_mean>0):
            #src es más clara que ref
            if(direction==-1):
                direction = 1
                step = step / 2
                
            gamma_i   = gamma_i - step
            if(gamma_i<=0):gamma_i=step
            
        src_corr  = adjust_gamma(src, gamma=gamma_i)

        frame_mean = cv2.mean(src_corr)[0]
        dif_mean  = frame_mean - cv2.mean(ref)[0]
        err       = abs(dif_mean)
        iteration = iteration - 1
        
        print("it["+str(max_iterations-iteration)+"] - gamma: " + str(round(gamma_i,2)) + "\t| err: " + str( round(err,2)))

        if(imShow):cv2.imshow('src_corr', src_corr)
        if(imShow):cv2.imshow('dif', cv2.absdiff(src_corr, ref))
        
        info = "it["+str(max_iterations-iteration)+"] - gamma: " + str(round(gamma_i,2)) + " | err: " + str( round(err,2)) + " | mean: " + str( round(cv2.mean(src_corr)[0],2))
        
    return src_corr

def Homography(img, prev_img):

    if prev_img is None:
        return

    orb = cv2.ORB_create()
    kpt1, des1 = orb.detectAndCompute(prev_img, None)
    kpt2, des2 = orb.detectAndCompute(img, None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key=lambda x: x.distance)

    src_pts = np.float32([kpt1[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)
    dst_pts = np.float32([kpt2[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    return M 

# bufferless VideoCapture
class VideoCapture:

  def __init__(self, name):
    self.cap = cv2.VideoCapture(name)
    self.q = queue.Queue()
    t = threading.Thread(target=self._reader)
    t.daemon = True
    t.start()

  # read frames as soon as they are available, keeping only most recent one
  def _reader(self):
    while True:
      ret, frame = self.cap.read()
      if not ret:
        break
      if not self.q.empty():
        try:
          self.q.get_nowait()   # discard previous (unprocessed) frame
        except queue.Empty:
          pass
      self.q.put(frame)

  def read(self):
    return self.q.get()

  def release(self):
      self.cap.release()


class FileVideoStream:
    def __init__(self, path, queueSize=128):
        # initialize the file video stream along with the boolean
        # used to indicate if the thread should be stopped or not
        self.stream = cv2.VideoCapture(path)
        self.stopped = False
        # initialize the queue used to store frames read from
        # the video file
        self.Q = queue.Queue(maxsize=queueSize)
    
    def start(self):
        # start a thread to read frames from the file video stream
        t = Thread(target=self.update, args=())
        t.daemon = True
        t.start()
        return self
    
    def update(self):
        # keep looping infinitely
        while True:
            # if the thread indicator variable is set, stop the
            # thread
            if self.stopped:
                return
            # otherwise, ensure the queue has room in it
            if not self.Q.full():
                # read the next frame from the file
                (grabbed, frame) = self.stream.read()
                # if the `grabbed` boolean is `False`, then we have
                # reached the end of the video file
                if not grabbed:
                    self.stop()
                    return
                # add the frame to the queue
                self.Q.put(frame)
    
    def read(self):
        # return next frame in the queue
        return self.Q.get()
    
    def more(self):
        # return True if there are still frames in the queue
        return self.Q.qsize() > 0
    
    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True


    
##########
## MAIN ##
#################################################################################################################################

print("openCv version: " + cv2.__version__)
print("python version: " + sys.version)


############################# DEMOS ###########################################

videoPath_online_0 = 'rtsp://admin:camara@193.168.0.152/'
frame_start_online_0=0

videoPath_online_1 = 'rtsp://admin:camara@193.168.0.145/'
frame_start_online_1=0

videoPath_online_2 = 'rtsp://admin:12345abc@192.168.2.43/'
frame_start_online_1=0

#DEMO 0
videoPath_0_0 ='../../videos_test/2020-05-20/ch01_00000000028000000_.mp4'
frame_start_0_0 = 54000


videoPath_0_1 ='../../videos_test/2020-05-20/ch01_00000000046000000_.mp4'
frame_start_0_1 = 0

videoPath_0_2 ='../../videos_test/2020-05-20/ch01_00000000000000000_.mp4'
frame_start_0_2 = 45000


#DEMO 1
videoPath_1_0 ='../../videos_test/2020-05-22/cam0/ch01_00000000036000000.mp4'
frame_start_1_0 = 3518

videoPath_1_1 ='../../videos_test/2020-05-22/cam1/ch01_00000000061000000.mp4'
frame_start_1_1 = 9444

videoPath_1_2 ='../../videos_test/2020-05-22/cam2cc/ch01_00000000086000000.mp4'
frame_start_1_2 = 14500

cnf_roi0_0 = '../../videos_test/2020-05-22/cnf_roi0_0'
cnf_roi0_1 = '../../videos_test/2020-05-22/cnf_roi0_1'
cnf_H_0 = '../../videos_test/2020-05-22/cnf_H_0'

#DEMO 2
videoPath_2_0 ='../../videos_test/2020-05-22/cam0/ch01_00000000037000000.mp4'
frame_start_2_0 = 5500

videoPath_2_1 ='../../videos_test/2020-05-22/cam1/ch01_00000000062000000.mp4'
frame_start_2_1 = 21300

videoPath_2_2 ='../../videos_test/2020-05-22/cam2/ch01_00000000000000000.mp4'
frame_start_2_2 = 1125

#DEMO 3
videoPath_3_0 ='../../videos_test/2020-05-22/cam0/ch01_00000000041000000.mp4'
frame_start_3_0 = 5500

videoPath_3_1 ='../../videos_test/2020-05-22/cam1/ch01_00000000067000000.mp4'
frame_start_3_1 = 21300

videoPath_3_2 ='../../videos_test/2020-05-22/cam2/ch01_00000000006000000.mp4'
frame_start_3_2 = 1125


###############################################################################

#SELECT DEMO

"""
videoPath0_ = videoPath_online_0
frame0_start = frame_start_online_0

videoPath1_ = videoPath_online_1
frame1_start = frame_start_online_1
"""

videoPath0_= videoPath_1_0
frame0_start = frame_start_1_0

videoPath1_= videoPath_1_1
frame1_start = frame_start_1_1

cnf_roi0_path = cnf_roi0_0
cnf_roi1_path = cnf_roi0_1
cnf_H_path    = cnf_H_0

cnf_roi0_path = None
cnf_roi1_path = None
cnf_H_path    = None


#LOAD CONFIG DATA IF EXIST
cnf_roi_0 = []
cnf_roi_1 = []
cnf_H = []

if(cnf_roi0_path!=None)and(os.path.exists(cnf_roi0_path)): cnf_roi_0 = np.loadtxt(cnf_roi0_path)
if(cnf_roi1_path!=None)and(os.path.exists(cnf_roi1_path)): cnf_roi_1 = np.loadtxt(cnf_roi1_path)
if(cnf_H_path!=None)and(os.path.exists(cnf_H_path)): cnf_H = np.loadtxt(cnf_H_path)


# cap0 = VideoCapture(videoPath0_)
# cap1 = VideoCapture(videoPath1_)

cap0 = cv2.VideoCapture(videoPath0_)

cap1 = cv2.VideoCapture(videoPath1_)
print("vp0:", videoPath0_)
print("vp1:", videoPath1_)


#printVideoStats(cap0)

#printVideoStats(cap1)


cap0.set(1, frame0_start)
cap1.set(1, frame1_start)

# img0_ref = cap0.read()
# img1_ref = cap1.read()

print("reading cap0")
ret0, img0_ref = cap0.read()
cv2.imshow("img0", img0_ref)
cv2.waitKey(0)
cv2.destroyAllWindows()

ret1, img1_ref = cap1.read()

fps0 = round(float(cap0.get(cv2.CAP_PROP_FPS)), 2)
fps1 = round(float(cap1.get(cv2.CAP_PROP_FPS)), 2)

frameId0 = cap0.get(1) #current frame number
frameId1 = cap1.get(1) #current frame number
    

#CHECK SYNQ
frame_synq_0 = 0
frame_synq_1 = 0
while True:
    frameId0 = cap0.get(1) #current frame number
    frameId1 = cap1.get(1) #current frame number
    
    ret0, img0 = cap0.read()
    ret1, img1 = cap1.read()
    
    
    frame_synq_0 = frame_synq_0 + 1
    frame_synq_1 = frame_synq_1 + 1

    if (frameId0 % math.floor(fps0) == 0):
        putFrameNumberLabel(img0, frameId0)  
    imshow_0_1 = cv2.hconcat((img0, img1))
    # cv2.imshow('VIDEO SYNQ', ResizeWithAspectRatio(imshow_0_1, int(1600)))
    cv2.imshow('img0', ResizeWithAspectRatio(img0, int(1980/3)))
    
    if (frameId1 % math.floor(fps1) == 0):
        putFrameNumberLabel(img1, frameId1)
    imshow_0_1 = cv2.hconcat((img0, img1))
    # cv2.imshow('VIDEO SYNQ', ResizeWithAspectRatio(imshow_0_1, int(1600)))
    cv2.imshow('img1', ResizeWithAspectRatio(img1, int(1980/3)))
        
    
    key = cv2.waitKey(1) & 0xFF      
    if key == ord('q'):
        cv2.destroyAllWindows()
        sys.exit()
        
    if key == ord('c'):
        cv2.destroyAllWindows()
        break
    
# cap0.set(1, frame0_start)
# cap1.set(1, frame1_start)

ret0, img0_ref = cap0.read()
ret1, img1_ref = cap1.read()

pointradius = 2
pointColor = (0, 0, 255)

roi_scale_percent = 40

homography_estimation_thresh = 0.8
RANSAC_REPROJ_THRESHOLD = 5.0

frame_skip = 25


######################## FIRST PHOTO ROI #############################

roi_imgRef = img0_ref

roi_pts = []

if(len(cnf_roi_0)==0):
    selectROI()
    img0_roi_points = np.array(roi_pts, np.int32).reshape((-1, 1, 2))
else:
    img0_roi_points = np.array(cnf_roi_0, np.int32).reshape((-1, 1, 2))
    
img0_roi_rect = cv2.boundingRect(img0_roi_points)
img0_roi = getRoiframe(img0_ref, img0_roi_points)

mask = np.zeros(img0_ref.shape, np.uint8)
mask = cv2.polylines(mask, [img0_roi_points], True, (255, 255, 255), 2)
img0_roi_mask = cv2.fillPoly(mask.copy(), [img0_roi_points], (255, 255, 255)) # for ROI
# img0_roi_mask_crop = img0_roi_mask[img0_roi_rect[1]: img0_roi_rect[1] + img0_roi_rect[3], img0_roi_rect[0]: img0_roi_rect[0] + img0_roi_rect[2]]

img0_roi_mask_crop_retvalbin, img0_roi_mask_crop = cv2.threshold(cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)

cv2.imshow('img0_roi', ResizeWithAspectRatio(img0_roi, int(1980/3)))
# cv2.imshow('img0_roi_mask_crop', ResizeWithAspectRatio(img0_roi_mask_crop, int(1980/2)))
# cv2.imshow('img0_roi_mask', ResizeWithAspectRatio(img0_roi_mask, int(1980/2)))

######################## SECOND PHOTO ROI #############################

roi_imgRef = img1_ref

roi_pts = []
if(len(cnf_roi_1)==0):
    selectROI()
    img1_roi_points = np.array(roi_pts, np.int32).reshape((-1, 1, 2))
else:
    img1_roi_points = np.array(cnf_roi_1, np.int32).reshape((-1, 1, 2))
    
img1_roi_rect = cv2.boundingRect(img1_roi_points)

img1_roi = getRoiframe(img1_ref, img1_roi_points)

mask = np.zeros(img1_ref.shape, np.uint8)
mask = cv2.polylines(mask, [img1_roi_points], True, (255, 255, 255), 2)
img1_roi_mask = cv2.fillPoly(mask.copy(), [img1_roi_points], (255, 255, 255)) # for ROI
# img1_roi_mask_crop = img1_roi_mask[img1_roi_rect[1]: img1_roi_rect[1] + img1_roi_rect[3], img1_roi_rect[0]: img1_roi_rect[0] + img1_roi_rect[2]]

img1_roi_mask_crop_retvalbin, img1_roi_mask_crop = cv2.threshold(cv2.cvtColor(img1_roi, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)

cv2.imshow('img1_roi', ResizeWithAspectRatio(img1_roi, int(1980/3)))
# cv2.imshow('img1_roi_mask_crop', ResizeWithAspectRatio(img1_roi_mask_crop, int(1980/2)))
# cv2.imshow('img1_roi_mask', ResizeWithAspectRatio(img1_roi_mask, int(1980/2)))

######################## HOMOGRAPHY ESTIMATION PHOTO ROI #############################
img_ = img0_roi
#img_ = cv2.resize(img_, (0,0), fx=1, fy=1)
img1 = cv2.cvtColor(img_,cv2.COLOR_BGR2GRAY)

img = img1_roi
#img = cv2.resize(img, (0,0), fx=1, fy=1)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)


# cv2.imshow('img1', ResizeWithAspectRatio(img1, int(1980/2)))
# cv2.imshow('img2', ResizeWithAspectRatio(img2, int(1980/2)))

sift = cv2.xfeatures2d.SIFT_create()
# find key points
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

img1_keypoints = cv2.drawKeypoints(img1, kp1,None)
img2_keypoints = cv2.drawKeypoints(img2, kp2,None)
cv2.imshow('img1_keypoints', ResizeWithAspectRatio(img1_keypoints, int(1980/3)))
cv2.imshow('img2_keypoints', ResizeWithAspectRatio(img2_keypoints, int(1980/3)))
#cv2.imshow('original_image_left_keypoints',cv2.drawKeypoints(img_,kp1,None))

#FLANN_INDEX_KDTREE = 0
#index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
#search_params = dict(checks = 50)
#match = cv2.FlannBasedMatcher(index_params, search_params)
match = cv2.BFMatcher()
matches = match.knnMatch(des1,des2,k=2)

good = []
good_it = 0
good_step = 0.05
good_coef = 0
estimationCoef = 0
estimationCoef_step = 0.05

MIN_MATCH_COUNT = int((homography_estimation_thresh - estimationCoef) *len(matches))

while len(good) < MIN_MATCH_COUNT:
    
    good = []
    good_it = 0
    
    MIN_MATCH_COUNT = int((homography_estimation_thresh - estimationCoef) *len(matches))
    print("total matches: " + str(len(matches)) + " - target: " + str(MIN_MATCH_COUNT))
    
    while len(good) < MIN_MATCH_COUNT:
        
        good_coef = good_step*(good_it+1)
        if(good_coef>=1):
            estimationCoef = estimationCoef + estimationCoef_step
            break
        
        for m,n in matches:
            if m.distance < good_coef*n.distance:
                good.append(m)
        good_it = good_it + 1
    
        print("it[" + str(good_it) +"] - good_coef[" + str(good_coef) + "] - good matches: " + str(len(good)) + "/" + str(MIN_MATCH_COUNT))
       
        if(len(good) < MIN_MATCH_COUNT): good = []
        
        
draw_params = dict(matchColor=(0,255,0), singlePointColor=None, flags=2)

img3 = cv2.drawMatchesKnn(img_,kp1,img,kp2,matches,None,flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
# cv2.imshow("matches", img3)

img3 = cv2.drawMatches(img_,kp1,img,kp2,good,None,**draw_params)
cv2.imshow("goods matches", img3)


if len(good) >= MIN_MATCH_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, RANSAC_REPROJ_THRESHOLD)

    h,w = img1.shape
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    dst = cv2.perspectiveTransform(pts, M)
    img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)
    #cv2.imshow("original_image_overlapping.jpg", img2)
else:
    print("Not enought matches are found - "+str(len(good))+"/"+str(MIN_MATCH_COUNT))
    cv2.destroyAllWindows()
    sys.exit()

if(len(cnf_H)>0):
    M = cnf_H

finalH = M
print("Final homography:\n" + str(finalH))

# save cnf data
cnf_H_path = "H_1_to_0_0_v1"

if(cnf_H_path!=None): np.savetxt(cnf_H_path,finalH) 
  
if(cnf_roi0_path!=None):
    pts_roi0=[]

    
    for pts in img0_roi_points:
        pts_roi0.append((pts[0,0], pts[0,1]))

    np.savetxt(cnf_roi0_path, pts_roi0)

if(cnf_roi1_path!=None):
    pts_roi1=[]
    
    for pts in img1_roi_points:
        pts_roi1.append((pts[0,0], pts[0,1]))
        
    np.savetxt(cnf_roi1_path, pts_roi1)
    
# print("Final inliers count: " + str(len(inliers)))
 
# matchImg = drawMatches(img0,kp1,img1,kp2,matches,inliers)
# cv2.imshow('InlierMatches',ResizeWithAspectRatio(matchImg, 1980))

############################# MATCH PHOTO #############################
src = img1_roi
dst = img0_roi
transf = finalH

# run provided algorithm
# using inverse map since the given homography is 1 to 3; need 3 to 1
# src_warped, dst_padded = padtransf.warpPerspectivePadded(src, dst, transf, flags=cv2.WARP_INVERSE_MAP)
# alpha = 0.5
# beta = 1 - alpha
# blended = cv2.addWeighted(src_warped, alpha, dst_padded, beta, 1.0)
# cv2.imshow("Blended warp, with padding", ResizeWithAspectRatio(blended,1000))
# cv2.waitKey(0)


crop_warped = cv2.warpPerspective(src, transf, (dst.shape[1], dst.shape[0]), flags=cv2.WARP_INVERSE_MAP)
alpha = 0.5
beta = 1 - alpha
blended = cv2.addWeighted(crop_warped, alpha, dst, beta, 1.0)

img1_roi_mask_warped_retvalbin, img1_roi_mask_warped = cv2.threshold(cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)
mask_intersection = cv2.bitwise_and(img0_roi_mask_crop, img1_roi_mask_warped)

# cv2.imshow("img1_roi_warped", ResizeWithAspectRatio(crop_warped,1000))
# cv2.imshow("img1_roi_mask_warped", ResizeWithAspectRatio(img1_roi_mask_warped,1000))
cv2.imshow("Blended warp, standard crop", ResizeWithAspectRatio(blended,100))

# cv2.imshow("mask_intersection", ResizeWithAspectRatio(mask_intersection,1000))


# result_img0_input = cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY)
# result_img1_input = cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY)

# imgCmp = cv2.absdiff(result_img0_input, result_img1_input)
# cv2.imshow('DIF = IMG0 - IMG1', ResizeWithAspectRatio(imgCmp, height = 500))

# test_retvalbin, test_bins = cv2.threshold(imgCmp, 10, 255, cv2.THRESH_BINARY)
# cv2.imshow('BINARY MASK', ResizeWithAspectRatio(test_bins, height = 500))



############################# MATCH PHOTO #############################
cv2.waitKey(0)
cv2.destroyAllWindows()

# TRACKBARS
# Create a black image, a window
cv2.namedWindow('Trackbars', cv2.WINDOW_NORMAL)
bin_thr_initial = 25
gauss_w_h_initial = 10
gauss_sigma_initial = 60

histogramEqu_en_name = 'Histograma'
cv2.createTrackbar(histogramEqu_en_name, 'Trackbars',0,1,nothing)
cv2.createTrackbar('bin_thr','Trackbars',bin_thr_initial,255,nothing)
cv2.createTrackbar('gauss_w_h','Trackbars',gauss_w_h_initial,100,nothing)
cv2.createTrackbar('gauss_sigma','Trackbars',gauss_sigma_initial,500,nothing)

result_img0_input = cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY)
result_img1_input = cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY)

# result_img1_input = cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY)

frame_n = 0
while(True):
    
    # result_img0_input = cap0.read()
    # result_img1_input = cap1.read()
    
    ret0, result_img0_input = cap0.read()
    ret1, result_img1_input = cap1.read()
    
    if ret0==0:
        break
        cap0.release()
        cap0 = cv2.VideoCapture(videoPath0_)
        continue
    if ret1==0:
        break
        cap1.release()
        cap1 = cv2.VideoCapture(videoPath1_)
        continue
    
    histogramEqu_en = cv2.getTrackbarPos(histogramEqu_en_name,'Trackbars')
    bin_thr = cv2.getTrackbarPos('bin_thr','Trackbars')
    gauss_sigma = cv2.getTrackbarPos('gauss_sigma','Trackbars')
    gauss_w_h_initial = 2*cv2.getTrackbarPos('gauss_w_h','Trackbars')+1
    gaussianSize_w = gauss_w_h_initial
    gaussianSize_h = gauss_w_h_initial
    frame_n = frame_n + 1
    
    if frame_n%frame_skip==0:
        
        img0_roi = getRoiframe(result_img0_input, img0_roi_points)
        img1_roi = getRoiframe(result_img1_input, img1_roi_points)
        
        img0_roi_gray = cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY)
        img1_roi_gray = cv2.cvtColor(img1_roi, cv2.COLOR_BGR2GRAY)
        
        src = img1_roi
        dst = img0_roi
        transf = finalH
    
        crop_warped = cv2.warpPerspective(src, transf, (dst.shape[1], dst.shape[0]), flags=cv2.WARP_INVERSE_MAP)
        alpha = 0.5
        beta = 1 - alpha
        blended = cv2.addWeighted(crop_warped, alpha, dst, beta, 1.0)
        putFrameNumberLabel(blended, frame_n)
        cv2.imshow("Blended warp, standard crop", ResizeWithAspectRatio(blended,1000))
        
        result_img0_input = cv2.cvtColor(img0_roi, cv2.COLOR_BGR2GRAY)
        result_img1_input = cv2.cvtColor(crop_warped, cv2.COLOR_BGR2GRAY)
        
        result_img0 = result_img0_input.copy()
        result_img1 = result_img1_input.copy()
        
        result_img0 = cv2.bitwise_and(result_img0, mask_intersection)
        result_img1 = cv2.bitwise_and(result_img1, mask_intersection)
        
        if(histogramEqu_en==1): result_img0 = cv2.equalizeHist(result_img0)
        if(histogramEqu_en==1): result_img1 = cv2.equalizeHist(result_img1)
    
        cv2.imshow('IMG0 TRANSFORMACION GEOM.', ResizeWithAspectRatio(result_img0, height = 500))
        cv2.imshow('IMG1 TRANSFORMACION GEOM.', ResizeWithAspectRatio(result_img1, height = 500))
        
        result_img0_gaussian = cv2.GaussianBlur(result_img0, (gaussianSize_w,gaussianSize_h), gauss_sigma)
        result_img1_gaussian = cv2.GaussianBlur(result_img1, (gaussianSize_w,gaussianSize_h), gauss_sigma)
    
        
        cv2.imshow('IMG0 GAUSS + HIST + BRILLO', ResizeWithAspectRatio(result_img0_gaussian, height = 500))
        # cv2.imshow('IMG1 GAUSS + HIST + BRILLO', ResizeWithAspectRatio(result_img1_gaussian, height = 500))
        
        max_iterations_ = 50
        result_img1 = autoAdjust_gama(result_img1.copy(), result_img0.copy(), step=0.1, err_min=0.1, max_iterations=max_iterations_)
        result_img1_gaussian = autoAdjust_gama(result_img1_gaussian.copy(), result_img0_gaussian.copy(), step=0.1, err_min=0.1, max_iterations=max_iterations_)
        
        # cv2.imshow('result_img1_brillo', ResizeWithAspectRatio(result_img1, height = 500))
        cv2.imshow('IMG1 GAUSS + HIST + BRILLO', ResizeWithAspectRatio(result_img1_gaussian, height = 500))
        
        # result_img0 = cv2.bitwise_and(result_img0, mask_intersection)
        # result_img1 = cv2.bitwise_and(result_img1, mask_intersection)
        
        # result_img0_gaussian = cv2.bitwise_and(result_img0_gaussian, mask_intersection)
        # result_img1_gaussian = cv2.bitwise_and(result_img1_gaussian, mask_intersection)
        
        # cv2.imshow('result_img0_intersection', ResizeWithAspectRatio(result_img0, height = 500))
        # cv2.imshow('result_img1_intersection', ResizeWithAspectRatio(result_img1, height = 500))
    
        
        imgCmp = cv2.absdiff(result_img0, result_img1)
        imgCmp_gaussian = cv2.absdiff(result_img0_gaussian, result_img1_gaussian)
        cv2.imshow('DIF = IMG0 - IMG1', ResizeWithAspectRatio(imgCmp_gaussian, height = 500))
        
        test_retvalbin, test_bins = cv2.threshold(imgCmp_gaussian, bin_thr, 255, cv2.THRESH_BINARY)
        cv2.imshow('BINARY MASK', ResizeWithAspectRatio(test_bins, height = 500))
        
        
        
        key = cv2.waitKey(1) & 0xFF      
        if key == ord('q'):
            break


cap0.release()
cap1.release()
cv2.destroyAllWindows()



