import cv2

def mostrar_camara(rtsp_url):
    # Abre la conexión con la cámara RTSP
    cap = cv2.VideoCapture(rtsp_url)

    while True:
        # Lee un frame de la cámara
        ret, frame = cap.read()

        if not ret:
            print("Error al leer el frame")
            break

        # Muestra el frame en una ventana
        cv2.imshow('Camara RTSP', frame)

        # Espera 1 milisegundo para detectar pulsaciones de teclas
        key = cv2.waitKey(1)

        # Si se presiona 'q', se cierra la ventana
        if key == ord('q'):
            break

    # Libera los recursos
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    # Dirección RTSP de la cámara
    rtsp_url = "rtsp://admin:1234abcd@192.168.1.64"
    rtsp_url = "rtsp://admin:clara222@192.168.1.65"

    # Llama a la función para mostrar la cámara
    mostrar_camara(rtsp_url)
